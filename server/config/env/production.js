const DB_HOST = "localhost", 
    DB_PASS = "r00t_XHm37146z-", 
    DB_USER  = "lachlan", 
    DB_NAME = "rmdb_prod", 
    DB_PORT = 3306;

const JWT_SECRET_KEY = '238944C2CCAB7F49B9D215F64CADE';

const UPLOADS_DIRECTORY = '/var/www/opensaucepan.com/api/uploads/posts';
const UPLOADS_ORIGINAL_DIRECTORY = '/var/www/opensaucepan.com/api/uploads/posts/original';
const UPLOADS_THUMBNAIL_DIRECTORY = '/var/www/opensaucepan.com/api/uploads/posts/thumbnails';

const URL = "http://www.opensaucepan.com";
const SSL_URL = "https://www.opensaucepan.com";

module.exports = {
    DB_HOST,
    DB_PASS,
    DB_USER,
    DB_NAME,
    DB_PORT,
    JWT_SECRET_KEY,
    UPLOADS_DIRECTORY,
    UPLOADS_ORIGINAL_DIRECTORY,
    UPLOADS_THUMBNAIL_DIRECTORY,
    URL,
    SSL_URL,
}