const DB_HOST = "localhost", 
    DB_PASS = "pass", 
    DB_USER  = "root", 
    DB_NAME = "rmdb", 
    DB_PORT = 3307;

const JWT_SECRET_KEY = '238944C2CCAB7F49B9D215F64CADE';

const UPLOADS_DIRECTORY = 'uploads/posts';
const UPLOADS_ORIGINAL_DIRECTORY = 'uploads/posts/original';
const UPLOADS_THUMBNAIL_DIRECTORY = 'uploads/posts/thumbnails';

const URL = 'http://localhost:3000';
const SSL_URL = 'https://localhost:3000';

module.exports = {
    DB_HOST,
    DB_PASS,
    DB_USER,
    DB_NAME,
    DB_PORT,
    JWT_SECRET_KEY,
    UPLOADS_DIRECTORY,
    UPLOADS_ORIGINAL_DIRECTORY,
    UPLOADS_THUMBNAIL_DIRECTORY,
    URL,
    SSL_URL,
}