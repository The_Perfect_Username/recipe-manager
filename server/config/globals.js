const prod = require('./env/production');
const dev = require('./env/development');

const isProd = process.env.NODE_ENV === "production";
const env = isProd ? prod : dev;

const DB_HOST        = env.DB_HOST;
const DB_PASS        = env.DB_PASS;
const DB_USER        = env.DB_USER;
const DB_NAME        = env.DB_NAME;
const DB_PORT        = env.DB_PORT;
const JWT_SECRET_KEY = env.JWT_SECRET_KEY;

const UPLOADS_DIRECTORY = env.UPLOADS_DIRECTORY;
const UPLOADS_ORIGINAL_DIRECTORY = env.UPLOADS_ORIGINAL_DIRECTORY;
const UPLOADS_THUMBNAIL_DIRECTORY = env.UPLOADS_THUMBNAIL_DIRECTORY;

const URL = env.URL;
const SSL_URL = env.SSL_URL;

module.exports = {
    DB_HOST,
    DB_PASS,
    DB_USER,
    DB_NAME,
    DB_PORT,
    JWT_SECRET_KEY,
    UPLOADS_DIRECTORY,
    UPLOADS_ORIGINAL_DIRECTORY,
    UPLOADS_THUMBNAIL_DIRECTORY,
    URL,
    SSL_URL,
}