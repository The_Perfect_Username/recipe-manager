const express = require("express");
const router  = express.Router();

const {
    createRating,
    deleteRating,
} = require("../lib/rating");

const {
    verifyUser,
} = require("../lib/auth");

router.post('/', verifyUser, (req, res) => {
    const payload = req.body;
    const payloadLength = Object.keys(payload).length;

    if (payloadLength === 0) {
        return res.status(422).json({ message: 'Payload is empty' });
    }

    const userId = req.user_id;
    const recipeId = payload.recipe_id || '';
    const rating = payload.rating || 0;

    if (recipeId.length === 0) {
        return res.status(422).json({ message: 'Recipe ID is missing' });
    }

    if (isNaN(rating)) {
        return res.status(422).json({ message: 'Rating must be a number' });
    }

    if (rating.toString().indexOf('.') !== -1) {
        return res.status(422).json({ message: 'Rating must be an integer' });
    }

    if (rating < 1 || rating > 5) {
        return res.status(422).json({ message: 'Rating must be between 1 - 5' });
    }

    // Set rating
    deleteRating(userId, recipeId, (error, _) => {
        if (error) {
            console.error(error);
            return res.status(error.code).json({ message: "Unable to update rating" });
        }

        createRating(userId, recipeId, rating, (error, _) => {
            if (error) {
                console.error(error);
                return res.status(error.code).json({ message: error.message });
            }

            res.sendStatus(204);
        });
    })
});

router.delete('/:id', verifyUser, (req, res) => {
    const params = req.params;
    const recipeId = params.id || '';

    if (recipeId.length === 0) {
        return res.status(422).json({ message: 'Recipe ID is missing' });
    }

    const userId = req.user_id;

    deleteRating(userId, recipeId, (error, _) => {
        if (error) {
            console.error(error);
            return res.status(error.code).json({ message: error.message });
        }

        res.sendStatus(204);
    });
});

module.exports = router;