const express        = require("express");
const router         = express.Router();

const {
    createComment,
    deleteComment,
    updateComment,
    getComments,
} = require('../lib/comments');

const {
    verifyUser,
    verifyCommentOwnership,
    noAuthRequired,
 } = require("../lib/auth");

router.get('/', noAuthRequired, (req, res) => {
    const query = req.query;
    const recipeId = query.recipe_id || null;
    const limit = query.limit || 25;
    const offset = query.offset || 0;
    const userId = req.user_id || null;

    if (!recipeId) return res.status(200).json({ results: [] });

    getComments(recipeId, limit, offset)
        .then(results => {
            const commentResults = results.results;
            const comments = commentResults.map((result) => {
                result.belongs_to_user = result.user_id === userId;
                return result;
            });

            res.status(200).json(comments)
        })
        .catch(error => res.status(500).json({ error }));
});

//TODO verify user
router.post('/', verifyUser, (req, res) => {

    const payload = req.body;

    if (Object.keys(payload).length === 0) {
        return res.status(422).json({ error: "Payload is empty"});
    }

    const userId = req.user_id;
    const recipeId = payload.recipe_id || '';
    const comment = payload.comment || '';

    if (!recipeId || recipeId.trim().length === 0) {
        console.error("Recipe ID is missing");
        return res.status(422).error({ error: "Was unable to create comment" });
    }

    if (!comment || comment.trim().length == 0) {
        return res.status(422).json({ comment_error: "Comment must not be left empty or blank" });
    } else if (comment.length > 500) {
        return res.status(422).json({ comment_error: "Comment must not exceed 500 characters" });
    }

    const params = {
        recipe_id: recipeId,
        user_id: userId,
        comment,
    }

    createComment(params)
        .then(results => res.status(200).json(results.results))
        .catch(error => res.status(500).json({ error }));
});

router.delete('/:id', verifyUser, verifyCommentOwnership, (req, res) => {

    const params = req.params;

    if (Object.keys(params).length === 0) {
        return res.status(422).json({ error: "Params are missing or empty"});
    }

    const commentId = params.id || '';

    if (!commentId || commentId.trim().length === 0) {
        console.error("Recipe ID is missing");
        return res.status(422).error({ error: "Was unable to delete comment" });
    }

    deleteComment(commentId)
        .then(results => res.sendStatus(results.code))
        .catch(error => res.status(500).json({ error }));
});

router.put('/:id', verifyUser, verifyCommentOwnership, (req, res) => {

    const params = req.params;
    const payload = req.body;

    if (Object.keys(params).length === 0) {
        return res.status(422).json({ error: "Params are missing or empty"});
    }

    if (Object.keys(payload).length === 0) {
        return res.status(422).json({ error: "payload is empty"});
    }

    const commentId = params.id || '';
    const comment = payload.comment || '';

    if (!commentId || commentId.trim().length === 0) {
        console.error("Comment Id is missing");
        return res.status(422).error({ error: "Was unable to update comment" });
    }

    if (!comment || comment.trim().length == 0) {
        return res.status(422).json({ comment_error: "Comment must not be left empty or blank" });
    } else if (comment.length > 500) {
        return res.status(422).json({ comment_error: "Comment must not exceed 500 characters" });
    }

    updateComment(commentId, comment)
        .then(results => res.sendStatus(results.code))
        .catch(error => res.status(500).json({ error }));
});

module.exports = router;