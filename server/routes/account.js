const express        = require("express");
const router         = express.Router();

const {
    verifyUser,
    noAuthRequired,
} = require("../lib/auth");

const {
    getAccountDetails,
    getAllRecipeFileNamesByUser,
    bulkDeleteRecipeImages,
} = require("../lib/account");

const {
    getAllRecipesFromUser,
    getAllRecipesFromUserByType,
    bulkDeleteRecipes,
} = require("../lib/recipe");

const {
    getRatedPosts,
} = require("../lib/rating");

const {
    getSavedPosts,
} = require("../lib/save");

const {
    verifyUserPassword,
    deleteAccount,
} = require("../lib/user");

router.get('/recipes', noAuthRequired, (req, res) => {
    let userId        = req.user_id;
    const query       = req.query;
    const accountId   = query.account_id || null;
    const type        = query.type || '';
    const limit       = query.limit || 12;
    const offset      = query.offset || 0;

    userId = accountId || userId;

    if (type === '') {
        getAllRecipesFromUser(userId, limit, offset, (error, results) => {
            if (error) {
                return res
                    .status(error.code)
                    .json({ message: error.message });
            }

            res.status(200).json(results.results);
        });
    } else {
        getAllRecipesFromUserByType(userId, type, limit, offset, (error, results) => {

            if (error) {
                return res
                    .status(error.code)
                    .json({ message: error.message });
            }

            res.status(200).json(results.results);
        });
    }
});

router.get('/rated', verifyUser, (req, res) => {
    let userId        = req.user_id;
    const query       = req.query;
    const accountId   = query.account_id || null;
    const page        = query.page || 0;
    const limit       = 12;
    const offset      = page * limit;

    userId = accountId || userId;

    getRatedPosts(userId, limit, offset, (error, results) => {

        if (error) {
            return res
                .status(error.code)
                .json({ message: error.message });
        }

        res.status(200).json(results.results);
    });

});

router.get('/saved', verifyUser, (req, res) => {
    let userId        = req.user_id;
    const query       = req.query;
    const accountId   = query.account_id || null;
    const page        = query.page || 0;
    const limit       = 12;
    const offset      = page * limit;

    userId = accountId || userId;

    getSavedPosts(userId, limit, offset, (error, results) => {

        if (error) {
            return res
                .status(error.code)
                .json({ message: error.message });
        }

        res.status(200).json(results.results);
    });

});


router.get('/', noAuthRequired, (req, res) => {
    const query = req.query;
    let userId = req.user_id || null;

    if (!userId && !query.account_id) {
        return res.status(422).json({ error: "Missing account id" });
    } else if (query.account_id) {
        userId = query.account_id;
    }

    getAccountDetails(userId, (error, results) => {
        if (error) {
            console.error(error);
            return res
                .status(error.code)
                .json({ message: error.message });
        }
        res.status(200).json(results.results);
    });
});

router.delete('/', verifyUser, (req, res) => {
    const payload = req.body;
    const payloadLenth = Object.keys(payload).length;

    if (payloadLenth === 0) {
        return res.status(422).json({ error: "Payload is empty" });
    }

    const userId = req.user_id;
    const password = payload.password || '';

    if (password.length === 0) {
        return res.status(401).json({ error: 'Incorrect password' });
    }

    verifyUserPassword(userId, password, (error, _) => {

        if (error) {
            return res.status(error.code).json({ error: error.message });
        }

        res.status(200).send(new Date());

        getAllRecipeFileNamesByUser(userId)
        .then(recipeFilenameArray => {
            bulkDeleteRecipeImages(recipeFilenameArray)
            .then(_ => {
                bulkDeleteRecipes(userId)
                .then(_ => `Successfully deleted recipes by user: ${userId}`)
                .catch(error => console.error(error));
            })
            .catch(error => console.error(error));
        });

        deleteAccount(userId)
        .then(_ => console.log(`Successfully deleted user account with id: ${userId}`))
        .catch(error => console.error(error));
    });
});

 module.exports = router;