const express        = require("express");
const router         = express.Router();
const multer         = require('multer');
const uuidv4         = require('uuid/v4');
const fs             = require('fs');

const {
    createThumbnail,
    moveFile,
    deleteFiles,
    formatFolderDirectoryFromId,
} = require('../lib/image');

const {
    timeToMinutes,
} = require("../lib/time");

const {
    verifyUser,
    verifyOwnership,
    noAuthRequired,
 } = require("../lib/auth");

const {
    createRecipe,
    getSingleRecipe,
    getSinglePost,
    updateRecipe,
    deleteRecipe,
    validateRecipePost,
    uploadFilter,
} = require("../lib/recipe");

const {
    UPLOADS_DIRECTORY,
    UPLOADS_ORIGINAL_DIRECTORY,
} = require("../config/globals");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const fileType = Object.keys(req.files).pop();

        if (fileType === 'file') {
            return cb(null, UPLOADS_DIRECTORY);
        }

        cb(null, UPLOADS_ORIGINAL_DIRECTORY);
    },
    filename: function (req, file, cb) {
        let extension  = file.originalname.split('.').pop();
        extension      = extension === 'jpeg' ? 'jpg' : extension;
        const filename = req.file_id + '.' + extension;
        req.filename   = filename;

        cb(null, filename);
    }
});

const upload = multer({
    storage,
    fileFilter: uploadFilter,
}).fields([
    {
        name: 'file',
        maxCount: 1,
    },
    {
        name: 'original_file',
        maxCount: 1,
    },
]);


router.get('/:id', noAuthRequired, (req, res) => {

    const params = req.params;
    const paramsLength = Object.keys(params).length;

    if (paramsLength === 0) {
        return res.status(400).json({ error: "URL params are missing" });
    }
    const userId = req.user_id || null;
    const recipeId = params.id;

    getSinglePost(recipeId, userId, (error, results) => {
        if (error) return res.status(error.code).json({ error: error.message });

        const payload = results.results;

        payload.belongs_to_user = results.results.user_id === userId;
        payload.saved = results.results.saved !== null;

        res.status(results.code).json(payload);
    });

});


router.post('/', verifyUser, (req, res) => {
    req.file_id = uuidv4();

    upload(req, res, (err) => {

        const payload = req.body;
        const payloadLength = Object.keys(payload).length;

        if (payloadLength === 0) {
            return res.status(400).json({ error: "Payload is empty" });
        }

        const userId            = req.user_id;
        const file              = req.files.file;
        const original_file     = req.files.original_file;

        if (!file || file.length === 0) {
            return res.status(422).json({ file_error: 'File is missing' });
        }

        if (!original_file || original_file.length === 0) {
            return res.status(422).json({ file_error: 'File is missing' });
        }

        const filename       = req.files.file.pop().filename;
        const name           = payload.name || '';
        const description    = payload.description || '';
        const ingredients    = payload.ingredients || '';
        const instructions   = payload.instructions || '';
        const privacy        = payload.privacy || '';

        let prepTime         = payload.prep_time || null;
        let cookTime         = payload.cook_time || null;
        let servings         = payload.servings || null;
        let calories         = payload.calories || null;

        let error = {};
        // No errors indicate that the fileFilter may not have fired because no file was provided
        if (!req.error) {
            validateRecipePost(name, description, ingredients, instructions, privacy, error);
        } else {
            error = req.error;
        }

        if (Object.keys(error).length > 0) {
            return res.status(422).json(error);
        }

        try {
            if (prepTime) {
                prepTime = timeToMinutes(prepTime);
            }

            if (cookTime) {
                cookTime = timeToMinutes(cookTime);
            }

            const fdir = formatFolderDirectoryFromId(filename);

            const fileSource = `${UPLOADS_DIRECTORY}/${filename}`;
            const fileDestination = `${UPLOADS_DIRECTORY}/${fdir}/${filename}`;
            const originalSource = `${UPLOADS_ORIGINAL_DIRECTORY}/${filename}`;
            const originalDestination = `${UPLOADS_ORIGINAL_DIRECTORY}/${fdir}/${filename}`;

            createThumbnail(fileSource);
            moveFile(fileSource, fileDestination);
            moveFile(originalSource, originalDestination);

            const params = {
                user_id: userId,
                name,
                description,
                ingredients,
                instructions,
                privacy,
                filename,
                servings: !servings || isNaN(servings) ? null : parseInt(servings),
                calories: !calories || isNaN(calories) ? null : parseInt(calories),
                prep_time: prepTime,
                cook_time: cookTime,
            }

            createRecipe(params, (error, results) => {
                if (error) return res.status(error.code).json({ error: error.message });

                res.status(results.code).json({ id: results.id });
            });
        } catch(error) {
            console.error(error);
            res.sendStatus(500);
        }

    });
});


router.post('/revision', verifyUser, (req, res) => {
    req.file_id = uuidv4();

    upload(req, res, (err) => {

        const payload = req.body;
        const payloadLength = Object.keys(payload).length;

        if (payloadLength === 0) {
            return res.status(400).json({ error: "Payload is empty" });
        }

        const userId       = req.user_id;
        const recipeId     = payload.id;
        const name         = payload.name || '';
        const description  = payload.description || '';
        const ingredients  = payload.ingredients || '';
        const instructions = payload.instructions || '';
        const privacy      = payload.privacy || '';
        const type         = 'revision';

        let prepTime       = payload.prep_time || null;
        let cookTime       = payload.cook_time || null;
        let servings       = payload.servings || null;
        let calories       = payload.calories || null;

        let error = {};

        if (!req.error) {
            validateRecipePost(name, description, ingredients, instructions, privacy, error);
        } else {
            error = req.error;
        }

        if (Object.keys(error).length > 0) {
            return res.status(422).json(error);
        }

        if (prepTime) {
            prepTime = timeToMinutes(prepTime);
        }

        if (cookTime) {
            cookTime = timeToMinutes(cookTime);
        }

        let params = {
            name,
            description,
            ingredients,
            instructions,
            privacy,
            type,
            parent: recipeId,
            user_id: userId,
            servings: !servings || isNaN(servings) ? null : parseInt(servings),
            calories: !calories || isNaN(calories) ? null : parseInt(calories),
            prep_time: prepTime,
            cook_time: cookTime,
        };

        const numberOfFiles = Object.keys(req.files).length;

        if (numberOfFiles > 0 && numberOfFiles !== 2) {
            const keys = Object.keys(req.files).join(', ');
            console.error(`Expected 2 files to be transmitted. Only recieved: ${keys}`);
            return res.sendStatus(422);
        }

        if (numberOfFiles > 0) {

            try {
                const filename = req.files.file.pop().filename;

                const fdir = formatFolderDirectoryFromId(filename);

                const fileSource = `${UPLOADS_DIRECTORY}/${filename}`;
                const fileDestination = `${UPLOADS_DIRECTORY}/${fdir}/${filename}`;
                const originalSource = `${UPLOADS_ORIGINAL_DIRECTORY}/${filename}`;
                const originalDestination = `${UPLOADS_ORIGINAL_DIRECTORY}/${fdir}/${filename}`;

                createThumbnail(fileSource);
                moveFile(fileSource, fileDestination);
                moveFile(originalSource, originalDestination);

                params.filename = filename;

                createRecipe(params, (error, results) => {
                    if (error) return res.status(error.code).json({ error: error.message });

                    res.status(results.code).json({ id: results.id });
                });

            } catch (error) {
                console.error(error);
                res.sendStatus(500);
            }
            return;
        }

        getSingleRecipe(recipeId, (error, results) => {
            if (error) {
                console.error(error);
                return res.status(error.code).json({ error: error.message });
            }

            try {
                const oldFilename = results.results.filename;
                const newFilename = uuidv4() + '.' + oldFilename.split('.').pop();

                const sdir = formatFolderDirectoryFromId(oldFilename);
                const ddir = formatFolderDirectoryFromId(newFilename);

                const source = `${UPLOADS_DIRECTORY}/${sdir}/${oldFilename}`;
                const destination = `${UPLOADS_DIRECTORY}/${ddir}/${newFilename}`;

                const originalSource = `${UPLOADS_ORIGINAL_DIRECTORY}/${sdir}/${oldFilename}`;
                const originalDestination = `${UPLOADS_ORIGINAL_DIRECTORY}/${ddir}/${newFilename}`;

                fs.mkdirSync(`${UPLOADS_DIRECTORY}/${ddir}`, { recursive: true });
                fs.mkdirSync(`${UPLOADS_ORIGINAL_DIRECTORY}/${ddir}`, { recursive: true });

                fs.copyFileSync(source, destination);
                fs.copyFileSync(originalSource, originalDestination);

                createThumbnail(destination);

                params.filename = newFilename;

                createRecipe(params, (error, results) => {
                    if (error) return res.status(error.code).json({ error: error.message });

                    res.status(results.code).json({ id: results.id });
                });
            } catch (error) {
                console.error(error);
                return res.status(409).json({ error: error.message });
            }
        });
    });
});

router.put('/:id', verifyUser, verifyOwnership, (req, res) => {
    req.file_id = uuidv4();

    upload(req, res, (err) => {
        const payload = req.body;
        const urlParams = req.params;
        const payloadLength = Object.keys(payload).length;
        const urlParamsLength = Object.keys(urlParams).length;

        if (payloadLength === 0) {
            return res.status(400).json({ error: "Payload is empty" });
        }

        if (urlParamsLength === 0) {
            return res.status(400).json({ error: "ID is missing" });
        }

        const recipeId     = urlParams.id;
        const name         = payload.name || '';
        const description  = payload.description || '';
        const ingredients  = payload.ingredients || '';
        const instructions = payload.instructions || '';
        const privacy      = payload.privacy || '';

        let prepTime       = payload.prep_time || null;
        let cookTime       = payload.cook_time || null;
        let servings       = payload.servings || null;
        let calories       = payload.calories || null;

        let error = {};

        if (!req.error) {
            validateRecipePost(name, description, ingredients, instructions, privacy, error);
        } else {
            error = req.error;
        }

        if (Object.keys(error).length > 0) {
            return res.status(422).json(error);
        }

        if (prepTime) {
            prepTime = timeToMinutes(prepTime);
        }

        if (cookTime) {
            cookTime = timeToMinutes(cookTime);
        }

        let params = {
            name,
            description,
            ingredients,
            instructions,
            privacy,
            servings: !servings || isNaN(servings) ? null : parseInt(servings),
            calories: !calories || isNaN(calories) ? null : parseInt(calories),
            prep_time: prepTime,
            cook_time: cookTime,
        };

        const numberOfFiles = Object.keys(req.files).length;

        if (numberOfFiles > 0 && numberOfFiles !== 2) {
            const keys = Object.keys(req.files).join(', ');
            console.error(`Expected 2 files to be transmitted. Only recieved: ${keys}`);
            return res.sendStatus(422);
        }

        if (numberOfFiles > 0) {
            try {
                const filename = req.files.file.pop().filename;
                const fdir = formatFolderDirectoryFromId(filename);
                const fileSource = `${UPLOADS_DIRECTORY}/${filename}`;
                const fileDestination = `${UPLOADS_DIRECTORY}/${fdir}/${filename}`;
                const originalSource = `${UPLOADS_ORIGINAL_DIRECTORY}/${filename}`;
                const originalDestination = `${UPLOADS_ORIGINAL_DIRECTORY}/${fdir}/${filename}`;

                createThumbnail(fileSource);
                moveFile(fileSource, fileDestination);
                moveFile(originalSource, originalDestination);

                params.filename = filename;

                // Get the recipe's original filename and use it to delete the files
                // before updating the records.
                getSingleRecipe(recipeId, (_, results) => {

                    const oldFilename = results.results.filename;

                    deleteFiles(oldFilename);

                    updateRecipe(recipeId, params, (error, results) => {
                        if (error) return res.status(error.code).json({ error: error.message });
                        res.status(results.code).json({ id: recipeId });
                    });
                });

            } catch (error) {
                console.error(error);
                res.sendStatus(500);
            }
            return;
        }
        updateRecipe(recipeId, params, (error, results) => {
            if (error) return res.status(error.code).json({ error: error.message });
            res.status(results.code).json({ id: recipeId });
        });
    });
});

router.delete('/:id', verifyUser, verifyOwnership, (req, res) => {
    const params = req.params;
    const payloadLenth = Object.keys(params).length;

    if (payloadLenth === 0) {
        return res.status(400).json({ error: "Id is missing" });
    }

    const recipeId = params.id || '';

    if (recipeId.length === 0) {
        return res.status(422).json({
            error: "Recipe ID is missing"
        });
    }

    try {

        getSingleRecipe(recipeId, (error, results) => {
            if (error) {
                console.error(error);
            }

            const filename = results.results.filename;

            deleteRecipe(recipeId, (error, results) => {
                if (error) return res.status(error.code).json({ error: error.message });
                deleteFiles(filename);
                res.sendStatus(results.code);
            });
        });

    } catch(error) {
        console.error(error);
        res.sendStatus(500);
    }

});

module.exports = router;