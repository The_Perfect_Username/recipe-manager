const express        = require("express");
const router         = express.Router();
const bcrypt         = require('bcrypt');
const { verifyUser } = require("../lib/auth");

const {
    createUserAccount,
    getUserAccount,
    updateUserAccount,
} = require('../lib/user');

router.get("/", verifyUser, (req, res) => {

    const userId = req.user_id;

    getUserAccount(userId, (error, results) => {
        if (error) return res.status(error.code).json({ message: error.message });

        res.status(200).json(results[0]);

    });
});

router.post("/", (req, res) => {
    const payload = req.body;
    const payloadLenth = Object.keys(payload).length;

    if (payloadLenth === 0) {
        return res.status(422).json({ message: "Payload is empty" });
    }

    const name = payload.name || null;
    const email = payload.email || null;
    const password = payload.password || null;

    let error = {};

    if (!name || name === "") {
        error.name_error = "Name must not be left empty";
    } else if (name.length > 50) {
        error.name_error = "Name must not exceed 50 characters";
    }

    if (!email || email === "") {
        error.email_error = "Email must not be left empty";
    } else if (email.indexOf('@') === -1) {
        error.email_error = "Email is invalid";
    } else if (email.length > 100) {
        error.email_error = "Email must not exceed 100 characters";
    }

    if (!password || password.length < 6) {
        error.password_error = "Password must have at least 6 characters";
    }

    if (Object.keys(error).length > 0) {
        return res.status(422).json(error);
    }

    createUserAccount(name, email, password, (error, results) => {
        if (error) {
            if (error.duplicate_error) {
                return res.status(error.code).json({ email_error: error.message });
            }
            return res.status(error.code).json({ message: error.message })
        };

        res.status(results.code).json({
            token: results.token,
            user: {
                name,
                email,
                id: results.id,
                display_name: '',
            }
        });
    });

});

router.put("/", verifyUser, (req, res) => {

    const payload = req.body;
    const payloadLenth = Object.keys(payload).length;

    if (payloadLenth === 0) {
        return res.status(400).json({ message: "Payload is empty" });
    }

    const userId = req.user_id;

    const name = payload.name || null;
    const display_name = payload.display_name || null;
    const email = payload.email || null;

    let error = {};

    if (!name || name === "") {
        error.name_error = "Name must not be left empty";
    } else if (name.length > 50) {
        error.name_error = "Name must not exceed 50 characters";
    }

    if (display_name && display_name.length > 15) {
        error.display_name_error = "Display name must not exceed 15 characters";
    }

    if (display_name && !display_name.match(/^[A-Za-z0-9_-]+$/)) {
        error.display_name_error = 'Display name must only contain alphanumeric characters and "-" and "_"';
    }

    if (!email || email === "") {
        error.email_error = "Email must not be left empty";
    }  else if (email.length > 100) {
        error.email_error = "Email must not exceed 100 characters";
    }

    if (email.indexOf('@') === -1) {
        error.email_error = "Email is invalid";
    }

    if (Object.keys(error).length > 0) {
        return res.status(422).json(error);
    }

    const params = {
        name: name,
        display_name: display_name,
        email: email,
    }

    updateUserAccount(userId, params, (error, results) => {
        if (error) return res.status(error.code).json({ account_error: error.message });

        return res.sendStatus(results.code);
    });
});

router.put("/password", verifyUser, (req, res) => {
    const payload = req.body;
    const payloadLenth = Object.keys(payload).length;

    if (payloadLenth === 0) {
        return res.status(400).json({ message: "Payload is empty" });
    }

    const userId = req.user_id;
    const password = payload.password || null;

    if (!password || password.length < 6) {
        return res.status(422).json({ password_error: "Password must have at least 6 characters" });
    }

    let hashedPassword = bcrypt.hashSync(password, 10);

    const params = {
        password: hashedPassword,
    }

    updateUserAccount(userId, params, (error, results) => {
        if (error) return res.status(error.code).json({ message: error.message });

        return res.sendStatus(results.code);
    });
});

module.exports = router;