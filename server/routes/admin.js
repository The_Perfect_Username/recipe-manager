const express        = require("express");
const router         = express.Router();

const {
    verifyUser,
} = require('../lib/auth');
const {
    fetchDataCount,
    verifyAdminLogin,
} = require('../lib/account');

router.get('/fetch', verifyUser, (req, res) => {
    const isAdmin = req.is_admin;

    if (!isAdmin) {
        return res.status(401).json({
            error: 'Unauthorised access'
        });
    }

    fetchDataCount()
    .then(response => {
        res.status(response.code).json(response.results);
    })
    .catch((error) => {
        res.status(error.code).json({ error: error.message });
    });
});

router.get('/login', (req, res) => {
    const body = req.body || {};
    const bodyLength = Object.keys(body).length;

    if (bodyLength === 0) {
        return res.status(422).json({ error: 'Body is missing or empty' });
    }

    const email = body.email || '';
    const password = body.password || '';

    verifyAdminLogin(email, password)
    .then(response => {
        const results = response.results[0];
        res.status(response.code).json({
            token: results.token,
            user: {
                id: results.id,
                name: results.name,
                email: results.email,
            }
        });
    }).catch(error => {
        res.status(error.code).json({ error: error.message });
    });
});

module.exports = router;