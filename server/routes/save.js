const express = require("express");
const router  = express.Router();

const {
    save,
    unsave,
} = require("../lib/save");

const {
    verifyUser,
} = require("../lib/auth");

router.post('/', verifyUser, (req, res) => {
    const payload = req.body;
    const payloadLength = Object.keys(payload).length;

    if (payloadLength === 0) {
        return res.status(422).json({ message: 'Payload is empty' });
    }

    const userId = req.user_id;
    const recipeId = payload.recipe_id || '';

    if (recipeId.length === 0) {
        return res.status(422).json({ message: 'Recipe ID is missing' });
    }

    unsave(userId, recipeId, (error, _) => {
        if (error) {
            console.error(error);
            return res.status(error.code).json({ message: error.message });
        }

        save(userId, recipeId, (error, _) => {
            if (error) {
                console.error(error);
                return res.status(error.code).json({ message: error.message });
            }

            res.sendStatus(204);
        });
    });
});

router.delete('/:id', verifyUser, (req, res) => {
    const params = req.params;
    const recipeId = params.id || '';

    if (recipeId.length === 0) {
        return res.status(422).json({ message: 'Recipe ID is missing' });
    }

    const userId = req.user_id;

    unsave(userId, recipeId, (error, _) => {
        if (error) {
            console.error(error);
            return res.status(error.code).json({ message: error.message });
        }

        res.sendStatus(204);
    });
});

module.exports = router;