const express = require("express");
const router  = express.Router();
const {
    getRecipes,
    searchRecipesByTitle,
    searchSuggestions,
} = require('../lib/search');

router.get('/', (req, res) => {
    const query = req.query;
    const page = query.page || 0;
    const limit = 12;

    let searchTerms = query.q || '';

    searchTerms = searchTerms.replace(/\*/g, '');

    if (searchTerms.trim() === '') {
        return res.status(200).json([]);
    }

    if (isNaN(page)) {
        return res
            .status(422)
            .json({
                message: "Page offset must be a number",
            });
    }

    const offset = parseInt(page) * limit;

    if (searchTerms.length === 0) {
        getRecipes(limit, offset, (error, results) => {
            if (error) {
                return res
                    .status(error.code)
                    .json({ message: error.message });
            }

            res.status(200).json(results.results);

        });
    } else {
        searchRecipesByTitle(searchTerms, limit, offset, (error, results) => {
            if (error) {
                return res
                    .status(error.code)
                    .json({ message: error.message });
            }

            res.status(200).json(results.results);

        });
    }
});

router.get('/suggestions', (req, res) => {
    const query = req.query;
    const limit = 7;

    let searchTerms = query.q || '';

    searchTerms = searchTerms.replace(/\*/g, '');

    if (searchTerms.trim() === '') {
        return res.status(200).json([]);
    }

    searchSuggestions(searchTerms, limit, (error, results) => {
        if (error) {
            return res
                .status(error.code)
                .json({ message: error.message });
        }

        res.status(200).json(results.results);
    });
});

module.exports = router;