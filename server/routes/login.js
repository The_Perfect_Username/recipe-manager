const express        = require("express");
const router         = express.Router();

const {
    verifyUserLogin,
} = require('../lib/user');


router.post("/", (req, res) => {
    const payload = req.body;
    const payloadLength = Object.keys(payload).length;

    if (payloadLength === 0) {
        return res.status(400).json({ message: "Payload is empty" });
    }

    const email = payload.email || null;
    const password = payload.password || null;

    verifyUserLogin(email, password, (error, results) => {
        if (error) return res.status(error.code).json({ error: error.message });

        return res.status(results.code).json({
            token: results.token,
            user: {
                id: results.id,
                name: results.name,
                email: results.email,
                display_name: results.display_name,
            }
        });
    });

});

module.exports = router;

