const http = require("http");
const express = require("express");
const bodyParser = require("body-parser");
const app = express();

const { URL, SSL_URL } = require("./config/globals");

const admin = require("./routes/admin");
const login = require("./routes/login");
const comments = require("./routes/comments");
const user = require("./routes/user");
const recipe = require("./routes/recipe");
const search = require("./routes/search");
const account = require("./routes/account");
const rating = require("./routes/rating");
const save = require("./routes/save");

const { formatFolderDirectoryFromId } = require('./lib/image');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function(req, res, next) {
    const allowedOrigins = [URL, SSL_URL];
    const origin = req.headers.origin;

    if (allowedOrigins.indexOf(origin) > -1) {
        res.header("Access-Control-Allow-Origin", origin);
    }

    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "DELETE, POST, PUT, GET, OPTIONS");
    next();
});

app.use('/admin', admin);
app.use('/login', login);
app.use('/comments', comments);
app.use('/user', user);
app.use('/recipe', recipe);
app.use('/search', search);
app.use('/account', account);
app.use('/rating', rating);
app.use('/save', save);

app.get('/image/:filename', (req, res) => {

    const params = req.params;
    const paramsLength = Object.keys(params).length;

    if (paramsLength === 0 || !('filename' in params)) {
        return res.status(422).json({ message: 'Missing file name'});
    }

    const fileDirectory = formatFolderDirectoryFromId(params.filename);
    const target = `./uploads/posts/${fileDirectory}/${params.filename}`;

    res.sendFile(target, { root: __dirname });

});

app.get('/image/original/:filename', (req, res) => {

    const params = req.params;
    const paramsLength = Object.keys(params).length;

    if (paramsLength === 0 || !('filename' in params)) {
        return res.status(422).json({ message: 'Missing file name'});
    }

    const fileDirectory = formatFolderDirectoryFromId(params.filename);
    const target = `./uploads/posts/original/${fileDirectory}/${params.filename}`;

    res.sendFile(target, { root: __dirname });

});

app.get('/image/thumbnail/:filename', (req, res) => {

    const params = req.params;
    const paramsLength = Object.keys(params).length;

    if (paramsLength === 0 || !('filename' in params)) {
        return res.status(422).json({ message: 'Missing file name'});
    }

    const fileDirectory = formatFolderDirectoryFromId(params.filename);
    const target = `./uploads/posts/thumbnails/${fileDirectory}/${params.filename}`;

    res.sendFile(target, { root: __dirname });

});

http.createServer(app).listen(3001);