const bcrypt                                  = require('bcrypt');
const connection                              = require("../config/db");
const jwt                                     = require("jsonwebtoken");
const sid                                     = require("shortid");
const uuidv4                                  = require('uuid/v4');
const { JWT_SECRET_KEY }                      = require("../config/globals");

const createUserAccount = (name, email, password, callback) => {

    const currentTime = new Date().toISOString().replace("T", " ").substr(0, 19);
    const id = sid.generate();
    const token = uuidv4();
    const saltRounds = 10;
    const hashedPassword = bcrypt.hashSync(password, saltRounds);

    const data = {
        id,
        name,
        email,
        token,
        password: hashedPassword,
        created: currentTime,
    }

    // Create new user account with phone number and auth code
    connection.query("INSERT INTO users SET ?", data, (error, _) => {
        if (error && error.code === 'ER_DUP_ENTRY') {
            console.error(error);
            return callback({
                code: 409,
                message: 'Email address already exists',
                duplicate_error: true,
            }, null);

        } else if (error) {
            console.error(error);
            return callback({
                code: 409,
                message: 'Failed to create user account',
                duplicate_error: true,
            }, null);
        }

        const createdJWT = jwt.sign({
            id,
            token,
        }, JWT_SECRET_KEY);

        callback(null, {
            id,
            code: 200,
            token: createdJWT,
        });
    });
}

const getUserAccount = (userId, callback) => {
    connection.query(
        "SELECT * FROM users WHERE id = ?",
        [userId],
        (error, results) => {

        if (error) {
            console.error(error);
            callback({code: 500, message: "An unexpected error occurred"}, null);
        }

        if (results.length === 0) {
            return callback({
                code: 404,
                message: "User does not exist"
            }, null);
        }

        callback(null, {
            code: 200,
            results: results[0]
        });
    });
}

const updateUserAccount = (userId, params, callback) => {

    if (typeof params !== "object") {
        return callback({
            code: 400,
            message: "Params argument must be a JSON object"
        }, null);
    }

    connection.query(
        "UPDATE users SET ? WHERE id = ?",
        [params, userId],
        (error, results) => {

        if (error) {
            console.error(error);

            let message = 'Failed to update user account';

            if (error.code === 'ER_DUP_ENTRY') {
                message = "Display name or email address already exists";
            }

            return callback({
                code: 409,
                message,
            }, null);
        }

        if (results.affectedRows === 0) {
            return callback({
                code: 409,
                message: "Failed to update user account"
            }, null);
        }

        callback(null, {
            code: 200
        });
    });
}

const verifyUserLogin = (email, password, callback) => {

    connection.query(
        "SELECT id, name, display_name, password, token, role FROM users WHERE email = ?",
        [email],
        (error, results) => {

        if (error) {
            console.error(error);
            return callback({
                code: 500,
                message:  "An unexpected error occurred",
            }, null);
        }

        if (results.length === 0) {
            return callback({
                code: 401,
                message: "Incorrect email/password combination",
            }, null);
        }

        const id = results[0].id;
        const name = results[0].name;
        const displayName = results[0].display_name;
        const hashedPassword = results[0].password;
        const token = results[0].token;
        const role = results[0].role;

        const isSame = bcrypt.compareSync(password, hashedPassword);

        if (!isSame) {
            return callback({
                code: 401,
                message: "Incorrect email/password combination",
            }, null);
        }

        if (role === 'banned') {
            return callback({
                code: 401,
                message: 'Account is banned for misuse',
            }, null);
        }

        const createdJWT = jwt.sign({
            id,
            token,
        }, JWT_SECRET_KEY);

        callback(null, {
            id,
            name,
            email,
            code: 200,
            token: createdJWT,
            display_name: displayName,
        });

    });

}

const verifyUserPassword = (userId, password, callback) => {
    connection.query("SELECT password FROM users WHERE id = ?", [userId], (error, results) => {

        if (error) {
            console.error(error);
            return callback({
                code: 404,
                message: 'User cannot be found',
            }, null);
        };

        const hashedPassword = results[0].password;

        const isSame = bcrypt.compareSync(password, hashedPassword);

        if (!isSame) {
            return callback({
                code: 401,
                message: "Password is incorrect",
            }, null);
        }

        callback(null, true);

    });
};

const deleteAccount = (userId) => {
    return new Promise((resolve, reject) => {
        connection.query("DELETE FROM users WHERE id = ?", [userId], (error, _) => {
            if (error) {
                reject(error)
            };

            resolve(true);
        });
    });
}

module.exports = {
    createUserAccount,
    getUserAccount,
    updateUserAccount,
    verifyUserLogin,
    verifyUserPassword,
    deleteAccount,
}