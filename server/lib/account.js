const connection = require("../config/db");

const {
    deleteFiles,
} = require('./image');

const getAccountDetails = (userId, callback) => {
    const query = `SELECT u.name AS user_name, u.display_name AS display_name, u.created AS created, r.type AS type,
        COUNT(r.type) AS count_of FROM users AS u LEFT JOIN recipes AS r ON u.id = r.user_id
        WHERE u.id = ? GROUP BY r.type`;

    connection.query(query, [userId], (error, results) => {
        if (error) {
            console.error(error);
            return callback({
                code: 409,
                message: "An unexpected error occurred"
            }, null);
        }

        let payload = {
            user_name: '',
            display_name: '',
            type_count: {
                all: 0,
                original: 0,
                revision: 0,
            },
            created: '',
        };

        results.forEach(r => {
            payload.type_count[r.type] = r.count_of;
        });

        payload.type_count.all = payload.type_count.original + payload.type_count.revision;

        if (results.length > 0) {
            payload.user_name = results[0].user_name;
            payload.display_name = results[0].display_name;
            payload.created = results[0].created;
        }

        callback(null, {
            code: 200,
            results: payload,
        });
    });
}

const getAllRecipeFileNamesByUser = (userId) => {
    return new Promise((resolve, reject) => {

        const query = "SELECT id, filename FROM recipes WHERE user_id = ?";

        connection.query(query, [userId], (error, results) => {
            if (error) {
                console.error(error);
                reject(error);
            }

            resolve(results);
        });

    });
}

const bulkDeleteRecipeImages = (images) => {
    return new Promise((resolve, reject) => {

        if (typeof images !== "object") {
            reject('Parameters must be of type object');
        }

        if (images.length === 0) {
            resolve(true);
        }

        images.forEach(image => {
            const filename = image.filename;
            deleteFiles(filename);
        });

        resolve(true);
    });
}

module.exports = {
    getAccountDetails,
    getAllRecipeFileNamesByUser,
    bulkDeleteRecipeImages,
}