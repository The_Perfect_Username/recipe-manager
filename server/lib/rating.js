const connection = require("../config/db");

const createRating = (userId, recipeId, rating, callback) => {

    const created = new Date().toISOString().replace("T", " ").substr(0, 19);

    const params = {
        recipe_id: recipeId,
        user_id: userId,
        rating,
        created,
    };

    const query = "INSERT INTO ratings SET ?";

    connection.query(query, params, (error) => {

        if (error) {
            console.error(error);
            return callback({
                code: 409,
                message: "Failed to create rating",
            }, null);
        }

        callback(null, {
            code: 200,
        });
    });
}

const deleteRating = (userId, recipeId, callback) => {
    const query = "DELETE FROM ratings WHERE recipe_id = ? AND user_id = ?";
    connection.query(query, [recipeId, userId], (error, _) => {
        if (error) {
            console.error(error);
            return callback({
                code: 409,
                message: "Failed to delete rating",
            }, null);
        }

        callback(null, {
            code: 200,
        });
    });
}

const getRatedPosts = (userId, limit, offset, callback) => {
    let query = "SELECT re.id AS id, (SELECT name FROM users WHERE id = re.user_id) AS user_name, (SELECT display_name";
    query += " FROM users WHERE id = re.user_id) AS display_name, re.name AS name, (SELECT AVG(rating)";
    query += " FROM ratings WHERE recipe_id = re.id) AS rating, re.filename AS filename, re.parent AS revision_parent,";
    query += " re.created AS created FROM recipes AS re LEFT JOIN ratings AS r ON r.recipe_id = re.id LEFT JOIN";
    query += " users AS u ON r.user_id = u.id WHERE r.user_id = ? AND re.privacy = \"public\" ORDER BY UNIX_TIMESTAMP(r.created)";
    query += " DESC LIMIT ? OFFSET ?";

    connection.query(query, [userId, parseInt(limit), parseInt(offset)], (error, results) => {
        if (error) {
            console.error(error);
            return callback({
                code: 409,
                message: "Failed retrieve rated recipes",
            }, null);
        }

        callback(null, {
            code: 200,
            results: results,
        });
    });
}

module.exports = {
    createRating,
    deleteRating,
    getRatedPosts,
}