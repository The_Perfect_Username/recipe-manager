const connection = require("../config/db");
const sid        = require("shortid");

const {
    minutesToTime,
} = require("../lib/time");

const createRecipe = (params, callback) => {
    const id = sid.generate();
    const created = new Date().toISOString().replace("T", " ").substr(0, 19);

    params.id = id;
    params.created = created;

    connection.query("INSERT INTO recipes SET ?", params, (error) => {

        if (error) {
            console.error(error);
            return callback({
                code: 409,
                message: "Failed to create recipe"
            }, null);
        }

        callback(null, {
            code: 200,
            id,
        });
    });
}

const getAllRecipesFromUser = (userId, limit, offset, callback) => {
    let query = "SELECT re.id AS id, u.name AS user_name, u.display_name AS display_name, re.name AS name, (SELECT AVG(rating)";
    query += " FROM ratings WHERE recipe_id = re.id) AS rating, re.filename AS filename, re.parent AS revision_parent, re.created AS created";
    query += " FROM recipes AS re LEFT JOIN users AS u ON re.user_id = u.id WHERE u.id = ?";
    query += " ORDER BY UNIX_TIMESTAMP(re.created) DESC LIMIT ? OFFSET ?";

    connection.query(query, [userId, parseInt(limit), parseInt(offset)], (error, results) => {
        if (error) {
            console.error(error);
            return callback({code: 500, message: "An unexpected error occurred"}, null);
        }

        if (results.length === 0) {
            return callback(null, {
                code: 204,
                results: [],
            });
        }

        callback(null, {
            code: 200,
            results: results
        });
    });
}

const getAllRecipesFromUserByType = (userId, type, limit, offset, callback) => {
    let query = "SELECT re.id AS id, u.name AS user_name, u.display_name AS display_name, re.name AS name, (SELECT AVG(rating)";
    query += " FROM ratings WHERE recipe_id = re.id) AS rating, re.filename AS filename, re.parent AS revision_parent, re.created AS created";
    query += " FROM recipes AS re LEFT JOIN users AS u ON re.user_id = u.id WHERE u.id = ? AND re.type = ?";
    query += " ORDER BY UNIX_TIMESTAMP(re.created) DESC LIMIT ? OFFSET ?";
    connection.query(query, [userId, type, parseInt(limit), parseInt(offset)], (error, results) => {
        if (error) {
            console.error(error);
            return callback({code: 500, message: "An unexpected error occurred"}, null);
        }

        if (results.length === 0) {
            return callback(null, {
                code: 204,
                results: [],
            });
        }

        callback(null, {
            code: 200,
            results: results
        });
    });
}

const getSingleRecipe = (recipeId, callback) => {

    connection.query("SELECT * FROM recipes WHERE id = ?", [recipeId], (error, results) => {

        if (error) {
            console.error(error);
            return callback({code: 500, message: "An unexpected error occurred"}, null);
        }

        if (results.length === 0) {
            return callback(null, {
                code: 204,
                results: {},
            });
        }

        callback(null, {
            code: 200,
            results: results[0]
        });
    });
}

const getSinglePost = (recipeId, userId, callback) => {
    let query = "SELECT re.id AS id, re.user_id AS user_id, u.name AS user_name, u.display_name AS display_name,";
    query += "  re.name AS name, re.description AS description, re.ingredients AS ingredients, re.instructions AS instructions,";
    query += " re.privacy AS privacy, re.prep_time AS prep_time, re.cook_time AS cook_time, re.servings AS servings, re.calories AS calories,";
    query += " AVG(r.rating) AS rating, (SELECT r.rating FROM ratings AS r WHERE r.user_id = ? AND r.recipe_id = ?)";
    query += " AS user_rating, re.parent AS revision_parent, (SELECT 1 FROM saved_recipes AS sr WHERE sr.user_id = ?";
    query += " AND sr.recipe_id = ?) AS saved, re.filename AS filename, re.created AS created FROM recipes AS re LEFT JOIN";
    query += " ratings AS r ON re.id = r.recipe_id RIGHT JOIN users AS u ON re.user_id = u.id WHERE re.id = ?";

    connection.query(query, [userId, recipeId, userId, recipeId, recipeId], (error, results) => {

        if (error) {
            console.error(error);
            return callback({code: 500, message: "An unexpected error occurred"}, null);
        }

        if (results.length === 0) {
            return callback(null, {
                code: 204,
                results: {},
            });
        }

        const prepTime = minutesToTime(results[0].prep_time);
        const cookTime = minutesToTime(results[0].cook_time);

        results[0].prep_time = prepTime;
        results[0].cook_time = cookTime;

        callback(null, {
            code: 200,
            results: results[0]
        });
    });
}

const updateRecipe = (recipeId, params, callback) => {

    if (typeof params !== "object") {
        return callback({
            code: 400,
            message: "Params argument must be a JSON object"
        }, null);
    }

    connection.query(
        "UPDATE recipes SET ? WHERE id = ?",
        [params, recipeId],
        (error, results) => {

        if (error) {
            console.error(error);
            return callback({
                code: 409,
                message:  "Failed to update recipe"
            }, null);
        }

        if (results.affectedRows === 0) {
            return callback({
                code: 409,
                message: "Failed to update recipe"
            }, null);
        }

        callback(null, {
            code: 200
        });
    });
}

const deleteRecipe = (recipeId, callback) => {

    connection.query(
        "DELETE FROM recipes WHERE id = ?",
        [recipeId],
        (error) => {

        if (error) {
            console.error(error);
            return callback({
                code: 409,
                message:  "Failed to delete recipe"
            }, null);
        }

        callback(null, {
            code: 204
        });
    });
}

const bulkDeleteRecipes = (userId) => {
    return new Promise((resolve, reject) => {
        const query = "DELETE FROM recipes WHERE user_id = ?";
        connection.query(query, [userId], (error, _) => {
            if (error) {
                reject(error);
            }

            resolve(true);
        });
    });
}

const validateRecipePost =  (name, description, ingredients, instructions, privacy, error) => {
    validateRecipeName(name, error);
    validateRecipeDescription(description, error);
    validateRecipeIngredients(ingredients, error);
    validateRecipeInstructions(instructions, error);
    validateRecipePrivacy(privacy, error);
}

const validateRecipeName = (name, error) => {
    if (!name || name.length === 0) {
        error.name_error = "Name must not be left empty" ;
    } else if (name.length > 100) {
       error.name_error = "Name must not exceed 100 characters";
    }
}

const validateRecipeDescription = (description, error) => {
    if (description && description.length > 240) {
        error.description_error = "Description must not exceed 240 characters";
    }
}

const validateRecipeIngredients = (ingredients, error) => {
    if (!ingredients || ingredients.length === 0) {
        error.ingredients_error = "Ingredients must not be left empty";
    } else if (ingredients.length > 2000) {
        error.ingredients_error = "Ingredients must not exceed 2000 characters";
    }
}

const validateRecipeInstructions = (instructions, error) => {
    if (!instructions || instructions.length === 0) {
        error.instructions_error = "Instructions must not be left empty";
    } else if (instructions.length > 2000) {
        error.instructions_error = "Instructions must not exceed 5000 characters";
    }
}

const validateRecipePrivacy = (privacy, error) => {
    const validPrivacyOptions = ['public', 'private'];

    if (!validPrivacyOptions.includes(privacy)) {
        error.privacyError = "Privacy option is invalid";
    }
}

const uploadFilter = (req, file, callback) => {

    try {
        const validImageExtensions = ["jpg", "jpeg", "png"];

        const fileNameArray = (file && file.originalname && file.originalname.split('.')) || [];
        const extension = fileNameArray.pop() || '';
        const megaByte = 1048576;
        const maxFileSize = megaByte * 4;

        let error = {};

        if (!file) {
            error.file_error = 'File is missing';
        } else if (file.size === 0) {
            error.file_error = 'File is empty';
        } else if (!validImageExtensions.includes(extension.toLowerCase())) {
            error.file_error = `Invalid file type: valid extensions include ${validImageExtensions.join(', ')}`;
        } else if (file.size > maxFileSize) {
            error.file_error = 'File must not exceed 4MB';
        }

        const payload      = req.body;
        const name         = payload.name || '';
        const description  = payload.email || '';
        const ingredients  = payload.ingredients || '';
        const instructions = payload.instructions || '';
        const privacy      = payload.privacy || '';

        validateRecipePost(name, description, ingredients, instructions, privacy, error);

        req.error = error;
        req.file = file;

        if (Object.keys(error).length > 0) {
            return callback(null, false);
        }

        callback(null, true);
    } catch(error) {
        console.error(error);
        callback(null, false);
    }


}

module.exports = {
    bulkDeleteRecipes,
    createRecipe,
    getAllRecipesFromUser,
    getAllRecipesFromUserByType,
    getSingleRecipe,
    getSinglePost,
    updateRecipe,
    deleteRecipe,
    validateRecipePost,
    validateRecipeName,
    validateRecipeDescription,
    validateRecipeIngredients,
    validateRecipeInstructions,
    validateRecipePrivacy,
    uploadFilter,
}