const connection = require("../config/db");
const sid        = require("shortid");

const createComment = (params) => {

    const id = sid.generate();
    const created = new Date().toISOString().replace("T", " ").substr(0, 19);

    params.id = id;
    params.created = created;

    return new Promise((resolve, reject) => {
        connection.query("INSERT INTO comments SET ?", params, (error) => {
            if (error) {
                console.error(error);
                reject("Was unable to create comment");
            };

            resolve({
                code: 200,
                results: params,
            });
        });
    });
}

const updateComment = (id, comment) => {
    return new Promise((resolve, reject) => {
        connection.query("UPDATE comments SET comment = ?, edited = 1 WHERE id = ?", [comment, id], (error) => {
            if (error) {
                console.error(error);
                reject("Was unable to update comment");
            };

            resolve({
                code: 204,
            });
        });
    });
}

const deleteComment = (id) => {
    return new Promise((resolve, reject) => {
        connection.query("DELETE FROM comments WHERE id = ?", [id], (error) => {
            if (error) {
                console.error(error);
                reject("Was unable to delete comment");
            };

            resolve({
                code: 204,
            });
        });
    });
}

const getComments = (recipeId, limit, offset) => {
    return new Promise((resolve, reject) => {
        let query = "SELECT c.id AS id, c.user_id AS user_id, u.name AS author, u.display_name AS display_name, c.comment AS comment,";
        query += " c.edited AS edited, c.created AS created FROM comments AS c LEFT JOIN users AS u ON c.user_id = u.id";
        query += " WHERE c.recipe_id = ? ORDER BY UNIX_TIMESTAMP(c.created) DESC LIMIT ? OFFSET ?";

        connection.query(query, [recipeId, parseInt(limit), parseInt(offset)], (error, results) => {
            if (error) {
                console.error(error);
                reject("Was unable to retrieve comments");
            };

            resolve({
                code: 200,
                results,
            });
        });
    });
}

module.exports = {
    createComment,
    deleteComment,
    updateComment,
    getComments,
}