const bcrypt                                  = require('bcrypt');
const jwt                                     = require("jsonwebtoken");
const connection                              = require("../config/db");
const { JWT_SECRET_KEY }                      = require("../config/globals");

const fetchDataCount = () => {
    return new Promise((resolve, reject) => {
        const query = `SELECT COUNT((SELECT 1 FROM users)) AS user_count,
        COUNT((SELECT 1 FROM recipes)) AS recipe_count, COUNT((SELECT 1 FROM comments)) AS comment_count`;

        connection.query(query, (error, results) => {
            if (error) {
                console.error(error);
                return reject({
                    code: 500,
                    message: 'An unexpected error occurred',
                });
            }

            resolve({
                code: 200,
                results,
            });
        });
    });
}

const verifyAdminLogin = (email, password) => {
    return new Promise((resolve, reject) => {
        connection.query(
            "SELECT id, name, password, token FROM users WHERE email = ? AND role = 'admin'",
            [email],
            (error, results) => {

            if (error) {
                console.error(error);
                return reject({
                    code: 500,
                    message:  "An unexpected error occurred",
                });
            }

            if (results.length === 0) {
                return reject({
                    code: 401,
                    message: "Incorrect email/password combination",
                });
            }

            const id = results[0].id;
            const name = results[0].name;
            const hashedPassword = results[0].password;
            const token = results[0].token;

            const isSame = bcrypt.compareSync(password, hashedPassword);

            if (!isSame) {
                return reject({
                    code: 401,
                    message: "Incorrect email/password combination",
                });
            }

            const createdJWT = jwt.sign({
                id,
                token,
            }, JWT_SECRET_KEY);

            resolve({
                id,
                name,
                email,
                code: 200,
                token: createdJWT,
            });
        });
    });
}

module.exports = {
    fetchDataCount,
    verifyAdminLogin,
}