const connection = require("../config/db");

const getRecipes = (limit, offset, callback) => {
    let query = "SELECT re.id AS id, u.name AS user_name, u.display_name AS display_name, re.name AS name, (SELECT AVG(rating)";
    query += " FROM ratings WHERE recipe_id = re.id) AS rating, re.filename AS filename, re.parent AS revision_parent, re.created AS created";
    query += " FROM recipes AS re LEFT JOIN users AS u ON re.user_id = u.id WHERE re.privacy = \"public\"";
    query += " ORDER BY UNIX_TIMESTAMP(re.created) DESC LIMIT ? OFFSET ?";

    connection.query(query, [limit, offset], (error, results) => {
        if (error) {
            console.log(error);
            return callback({
                code: 500,
                message: 'An unexpected error occurred',
            }, null);
        }

        callback(null, {
            code: 200,
            results,
        });
    });
}

const searchRecipesByTitle = (searchTerms, limit, offset, callback) => {
    const terms = `${searchTerms}`;;

    let query = "SELECT MATCH(re.name) AGAINST(? IN NATURAL LANGUAGE MODE) AS relevance, re.id AS id, u.name AS user_name, u.display_name AS display_name, re.name AS name, (SELECT AVG(rating)";
    query += " FROM ratings WHERE recipe_id = re.id) AS rating, re.filename AS filename, re.parent AS revision_parent, re.created AS created";
    query += " FROM recipes AS re LEFT JOIN users AS u ON re.user_id = u.id WHERE MATCH(re.name) AGAINST(? IN NATURAL LANGUAGE MODE) AND re.privacy = \"public\"";
    query += " ORDER BY relevance DESC LIMIT ? OFFSET ?";

    connection.query(query, [terms, terms, limit, offset], (error, results) => {
        if (error) {
            console.log(error);
            return callback({
                code: 500,
                message: 'An unexpected error occurred',
            }, null);
        }

        callback(null, {
            code: 200,
            results,
        });
    });
}

const searchSuggestions = (searchTerms, limit, callback) => {
    const terms = `${searchTerms.trim().replace(' ', '* ')}*`;
    let query = "SELECT DISTINCT name, MATCH(name) AGAINST(? IN BOOLEAN MODE) AS relevance FROM recipes WHERE MATCH(name)";
    query += " AGAINST(? IN BOOLEAN MODE) AND privacy = \"public\" ORDER BY relevance DESC LIMIT ?";

    connection.query(query, [terms, terms, limit], (error, results) => {
        if (error) {
            console.log(error);
            return callback({
                code: 500,
                message: 'An unexpected error occurred',
            }, null);
        }

        callback(null, {
            code: 200,
            results,
        });
    });
}

module.exports = {
    getRecipes,
    searchRecipesByTitle,
    searchSuggestions,
}