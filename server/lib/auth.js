const { JWT_SECRET_KEY } = require("../config/globals");
const jwt = require("jsonwebtoken");
const sid = require('shortid');
const connection = require("../config/db");

const verifyUser = (req, res, next) => {

    const authorization = req.headers ? (req.headers.authorization || null) : null;

    if (!authorization || authorization.indexOf("Bearer") === -1) {
        return res.status(403).send({ message: "Bad Authorization Header" });
    }

    const authHeaderData = authorization.split(' ');

    if (authHeaderData.length !== 2) {
        return res.status(401).json({ message: "Bad Authorization Header" });
    }

    try {
        const token = authHeaderData[1];
        // Decode token
        const jwtDecoded = jwt.verify(token, JWT_SECRET_KEY);

        if (Object.keys(jwtDecoded).length === 1)
            throw Error("No data found inside of decoded JWT");

        // Get user_id/email/password
        const id = jwtDecoded.id || null;
        const verificationToken = jwtDecoded.token || null;

        // Validate token information
        if (!id || !sid.isValid(id)) {
            throw Error("User ID is invalid");
        }

        if (!verificationToken) {
            throw Error("Verification token is missing");
        }

        connection.query("SELECT 1, role FROM users WHERE id = ? AND token = ?",
            [id, verificationToken],
            (error, results) => {

            if (error) {
                console.error(error);
                return res.status(500).json({ message: "An unxpected database error occurred" });
            }

            if (results.length === 0) {
                return res.status(401).json({ message: "Unable to authorise user" });
            }

            const role = results[0].role;

            if (role === 'banned') {
                return res.status(403).json({ message: 'Account is banned for misuse', is_banned: true, });
            }

            req.user_id = id;
            req.is_admin = role === 'admin';

            next();

        });

    } catch(error) {
        console.error(error.message);
        return res.status(401).json({ message: "Unable to verify token" });
    }

}

const verifyOwnership = (req, res, next) => {
    const params = req.params

    const userId = req.user_id;
    const recipeId = params.id || null;

    if (!recipeId) {
        console.error("Post ID is missing");
        return res.status(422).json({ message: "Post ID is missing "});
    }

    try {
        connection.query("SELECT 1 FROM recipes WHERE id = ? AND user_id = ?",
            [recipeId, userId],
            (error, results) => {

            if (error) {
                console.error(error);
                return res.status(500).json({ message: "An unxpected database error occurred" });
            }

            if (results.length === 0) {
                return res.status(401).json({ message: "Unable to authorise user" });
            }

            next();

        });
    } catch(error) {
        console.error(error.message);
        return res.status(401).json({ message: "Unable to verify ownership" });
    }

}

const verifyCommentOwnership = (req, res, next) => {
    const params = req.params
    const userId = req.user_id;
    const commentId = params.id || '';

    try {
        connection.query("SELECT 1 FROM comments WHERE id = ? AND user_id = ?",
            [commentId, userId],
            (error, results) => {

            if (error) {
                console.error(error);
                return res.status(500).json({ message: "An unxpected database error occurred" });
            }

            if (results.length === 0) {
                return res.status(401).json({ message: "Unable to authorise user" });
            }

            next();

        });
    } catch(error) {
        console.error(error.message);
        return res.status(401).json({ message: "Unable to verify ownership" });
    }

}

const noAuthRequired = (req, res, next) => {
    const authorization = req.headers ? (req.headers.authorization || null) : null;

    if (!authorization) return next();

    verifyUser(req, res, next);
}

module.exports = {
    verifyOwnership,
    verifyCommentOwnership,
    verifyUser,
    noAuthRequired,
};