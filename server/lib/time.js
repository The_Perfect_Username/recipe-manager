const timeToMinutes = (timeString) => {
    if (!timeString.match(/^[0-9]{2}:[0-9]{2}$/g)) {
        return null;
    }

    const minutesInAnHour = 60;
    const array = timeString.split(':');
    const hours = parseInt(array[0]);
    const minutes = parseInt(array[1]);

    return (hours * minutesInAnHour) + minutes;
}

const minutesToTime = (minutes) => {
    if (!minutes) return null;

    const minutesInAnHour = 60;

    if (minutes < minutesInAnHour) {
        return `00:${minutes}`;
    }

    const remainderMinutes = minutes % minutesInAnHour;
    const formattedMinutes = remainderMinutes < 10 ? `0${remainderMinutes}` : `${remainderMinutes}`;
    const hours = (minutes - remainderMinutes) / minutesInAnHour;
    const formattedHours = hours < 10 ? `0${hours}` : `${hours}`;

    return `${formattedHours}:${formattedMinutes}`;
}

module.exports = {
    timeToMinutes,
    minutesToTime,
}