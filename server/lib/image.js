const fs = require('fs');
const jimp = require('jimp');
const sizeOf = require('image-size');
const mv = require('mv');

const {
    UPLOADS_DIRECTORY,
    UPLOADS_ORIGINAL_DIRECTORY,
    UPLOADS_THUMBNAIL_DIRECTORY,
} = require("../config/globals");

const createThumbnail = async (fileDirectory) => {
    try {
        const dimensions = sizeOf(fileDirectory);
        const fileName = fileDirectory.split('/').pop();
        const height = dimensions.height;
        const maxHeightInPixels = 400;
        const setHeight = height > maxHeightInPixels ? maxHeightInPixels : height;

        const fdir = formatFolderDirectoryFromId(fileName);

        fs.mkdirSync(`${UPLOADS_THUMBNAIL_DIRECTORY}/${fdir}`, {recursive: true});

        jimp.read(fileDirectory)
            .then(img => {
                return img
                    .resize(jimp.AUTO, setHeight) // resize
                    .write(`${UPLOADS_THUMBNAIL_DIRECTORY}/${fdir}/${fileName}`); // save
            })
            .catch(err => {
                console.error(err);
            });
    } catch(error) {
        console.error(error);
        throw error;
    }
}

const formatFolderDirectoryFromId = (id) => {
    try {

        if (!id || id.length === 0) {
            throw Error("Image ID is missing");
        }

        const firstEightCharacters = id.split('-').shift();

        let folderStructureArray = [];

        for (let i = 0; i < 6; i += 2) {
            folderStructureArray.push(
                firstEightCharacters.substring(i, i + 2)
            );
        }

        return folderStructureArray.join('/');
    } catch(error) {
        console.error(error);
        throw error;
    }

}

const moveFile = async (source, destination) => {
    try {
        const filename = destination.split('/').pop();
        const destinationDirectory = destination.replace(`/${filename}`, '');

        fs.mkdirSync(destinationDirectory, { recursive: true });

        mv(source, destination, (error) => {
            if (error) throw Error(error);
        });

    } catch(error) {
        console.error(error);
    }
}

const deleteFiles = (filename) => {
    try {

        const fdir = formatFolderDirectoryFromId(filename);
        const file = `${UPLOADS_DIRECTORY}/${fdir}/${filename}`;
        const fileThumbnail = `${UPLOADS_THUMBNAIL_DIRECTORY}/${fdir}/${filename}`;
        const originalFile = `${UPLOADS_ORIGINAL_DIRECTORY}/${fdir}/${filename}`;

        fs.unlinkSync(file);
        fs.unlinkSync(fileThumbnail);
        fs.unlinkSync(originalFile);

    } catch(error) {
        console.error(error);
    }
}

module.exports = {
    createThumbnail,
    formatFolderDirectoryFromId,
    moveFile,
    deleteFiles,
};