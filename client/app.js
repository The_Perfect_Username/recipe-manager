const http = require("http");
const express = require("express");
const path = require("path");
const app = express();

app.use(express.static(path.join(__dirname, 'client/build')));

app.get('*', (req, res) =>{
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

const port = process.env.CLIENT_PORT || 3000;

http.createServer(app).listen(port);