import React from 'react';
import { Switch, Route } from 'react-router-dom';

import EditAccount from './Components/Account/EditAccount';
import Account from './Components/Account/Account';
import { RegistrationForm, LoginForm } from './Components/Entry/Entry';
import FrontPage from './Components/Home/Splash';
import EntryLayout from './Components/Layouts/Entry';
import Error404 from './Components/Layouts/Error404';
import MainLayout from './Components/Layouts/Main';
import PrivacyPolicy from './Components/PrivacyPolicy';
import Post from './Components/Recipe/Recipe';
import CreateRecipe from './Components/Recipe/Forms/CreateRecipe';
import CreateRevision from './Components/Recipe/Forms/CreateRevision';
import EditRecipe from './Components/Recipe/Forms/EditRecipe';
import ResultsGrid from './Components/Results/ResultsGrid';
import AuthenticationRequired, { AvoidEntry } from './utils/Auth';

import './App.scss';

const App: React.FC = () => {
	return (
		<Switch>
			<Route exact path="/" render={(matchProps) => <FrontPage {...matchProps} />} />
			<Route exact path="/privacy-policy" component={PrivacyPolicy} />
			<Route exact path="/search" render={() => MainLayout(ResultsGrid)} />
			<Route exact path="/signup" render={() => AvoidEntry(EntryLayout(RegistrationForm))} />
			<Route exact path="/signin" render={() => AvoidEntry(EntryLayout(LoginForm))} />
			<Route exact path="/post/create" render={() => AuthenticationRequired(
				MainLayout(CreateRecipe)
			)} />
			<Route exact path="/post/edit" render={(matchProps) => AuthenticationRequired(
				MainLayout(EditRecipe, matchProps)
			)} />
			<Route exact path="/post/revision" render={(matchProps) => AuthenticationRequired(
				MainLayout(CreateRevision, matchProps)
			)} />
			<Route exact path="/post/:id" render={(matchProps) => MainLayout(Post, matchProps)} />
			<Route exact path="/user/:id" render={(matchProps) => MainLayout(Account, matchProps)} />
			<Route exact path="/account" render={(matchProps) => AuthenticationRequired(
				MainLayout(Account, matchProps)
			)} />
			<Route exact path="/account/edit" render={() => AuthenticationRequired(
				MainLayout(EditAccount)
			)} />
			<Route component={Error404} />
		</Switch>
	);
}

export default App;
