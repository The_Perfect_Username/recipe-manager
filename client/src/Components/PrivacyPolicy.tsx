import React from 'react';

const PrivacyPolicy = () => (
    <div id="privacy-policy">
        <h1>Privacy Policy</h1>
        <div>
            <p>
                Open Saucepan (referred to as "Open Saucepan", "us", "we" or "our") is committed to providing quality services
                to you and this policy outlines our ongoing obligations to you in respect of how we manage your Personal Information.
            </p>
            <p>
                We have adopted the Australian Privacy Principles (APPs) contained in the Privacy Act 1988 (Cth) (the Privacy Act).
                The NPPs govern the way in which we collect, use, disclose, store, secure and dispose of your Personal Information.
            </p>
            <p>
                A copy of the Australian Privacy Principles may be obtained from the website of The Office of the Australian
                Information Commissioner at www.aoic.gov.au
            </p>
        </div>
        <div>
            <h2>What is Personal Information and why do we collect it?</h2>
            <p>
                Personal Information is information or an opinion that identifies an individual. Examples of Personal
                Information we collect include: names, user names, and email addresses.
            </p>
            <p>
                This Personal Information is obtained via our website www.opensaucepan.com and from third parties.
                We don’t guarantee website links or policy of authorised third parties.
            </p>
            <p>
                We collect your Personal Information for the primary purpose of providing our services to you, providing
                information to our clients and marketing. We may also use your Personal Information for secondary purposes
                closely related to the primary purpose, in circumstances where you would reasonably expect such use or disclosure.
            </p>
            <p>
                When we collect Personal Information we will, where appropriate and where possible, explain to you why we are
                collecting the information and how we plan to use it.
            </p>
        </div>
        <div>
            <h2>Sensitive Information</h2>
            <p>
                Sensitive information is defined in the Privacy Act to include information or opinion about such
                things as an individual's racial or ethnic origin, political opinions, membership of a political association,
                religious or philosophical beliefs, membership of a trade union or other professional body, criminal record
                or health information.
            </p>
            <p>
                Due to the nature of our website we do not willingly collect any sensitive information.
            </p>
        </div>
        <div>
            <h2>Third Parties</h2>
            <p>
                Where reasonable and practicable to do so, we will collect your Personal Information only from you. However,
                in some circumstances we may be provided with information by third parties. In such a case we will take
                reasonable steps to ensure that you are made aware of the information provided to us by the third party.
            </p>
        </div>
        <div>
            <h2>Disclosure of Personal Information</h2>
            <p>Your Personal Information may be disclosed in a number of circumstances including the following:</p>
            <ul>
                <li>Third parties where you consent to the use or disclosure; and</li>
                <li>Where required or authorised by law.</li>
            </ul>
        </div>
        <div>
            <h2>Security of Personal Information</h2>
            <p>
                Your Personal Information is stored in a manner that reasonably protects it from misuse and loss and from
                unauthorized access, modification or disclosure.
            </p>
            <p>
                When your Personal Information is no longer needed for the purpose for which it was obtained, we will take
                reasonable steps to destroy or permanently de-identify your Personal Information. If you wish to have your
                account deleted and subsequently all Personal Information destroyed, you may do so through your account.
            </p>
        </div>
        <div>
            <h2>Policy Updates</h2>
            <p>This Policy may change from time to time and is available on our website.</p>
        </div>
    </div>
);

export default PrivacyPolicy;
