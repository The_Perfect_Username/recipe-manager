import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Dropdown extends Component<{children: any, menu: Array<any>}> {
    state = {
        menu: [],
    }

    componentWillReceiveProps(props: any) {
        this.setState({ ...props });
    }

    componentWillMount() {
        this.setState({
            menu: this.props.menu,
        });
    }

    ignoreClick = (e: any) => {
        e.stopPropagation();
        resetDropdowns();
    }

    toggleDropdown = (e: any) => {
        e.preventDefault();
        e.stopPropagation();

        const btn = e.currentTarget;
        const parent = btn.parentNode;
        const classes = parent.classList;

        if (classes.contains("active")) {
            parent.classList.remove("active");
        } else {
            resetDropdowns();
            parent.classList.add("active");
        }
    }

    _renderTriggerButton = () => {
        return React.cloneElement(this.props.children, {
            onClick: this.toggleDropdown,
        });
    }

    render() {
        return (
            <div className="dropdown" onClick={this.ignoreClick}>
                {this._renderTriggerButton()}
                <div className="dropdown-menu">
                    {
                        this.state.menu.map((item: any, index) => {
                            const providedClasses = item.className || [];
                            const classNames = ['dd-item', ...providedClasses];
                            return (
                                <Link
                                    className={classNames.join(' ')}
                                    key={index}
                                    to={{
                                        pathname: item.pathname,
                                        state: item.data,
                                    }}
                                    onClick={item.action}
                                >
                                    {item.label}
                                </Link>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}

const resetDropdowns = () => {
	const dropdowns = document.querySelectorAll('.dropdown');
	dropdowns.forEach(element => {
		element.classList.remove('active');
	});
}
