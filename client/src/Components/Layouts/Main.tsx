import React from 'react';
import Header from './Header';

const MainLayout = (Component: any, matchProps?: any) => {
	return (
		<div onClick={resetDropdowns}>
			<Header />
			<main>
				<section className="main-content">
					<Component {...matchProps} />
				</section>
			</main>
		</div>
	)
}

const resetDropdowns = (e: any) => {
	const dropdowns = document.querySelectorAll('.dropdown');
	dropdowns.forEach(element => {
		element.classList.remove('active');
	});
}

export default MainLayout;