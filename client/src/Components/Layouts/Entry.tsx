import React from 'react';
import Header from './Header';

const EntryLayout = (Component: any) => (
	<>
		<Header />
		<main className="entry-pages">
			<section className="main-content">
				{<Component />}
			</section>
		</main>
	</>
)

export default EntryLayout;