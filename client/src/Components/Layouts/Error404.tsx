import React from 'react';
import SearchBar from './Search/SearchBar';

const Error404 = () => (
    <div id="error-page">
        <div>
            <div className="error-text">
                <h1>404</h1>
                <h2>The page you were looking for was not found!</h2>
            </div>
            <SearchBar placeholder="Try searching for a recipe..." />
        </div>
    </div>
);

export default Error404;