import React, { Component } from 'react';
import { RouteComponentProps, withRouter } from "react-router";
import { API_URL } from '../../Form/Form';

type Results = { name: string };

interface IState {
	search: string;
	placeholder: string;
	suggestions: Results[];
	activeSuggestion: number;
	fetching: boolean;
	isOpen: boolean;
};

class SearchBar extends Component<RouteComponentProps<any> & any> {
	private timer: any;
	private input: any;
	private searchBar: any;

	constructor(props: any) {
		super(props);
		this.timer = null;
		this.input = React.createRef();
		this.searchBar = React.createRef();
	}

	state: IState = {
		search: '',
		placeholder: '',
		suggestions: [],
		activeSuggestion: 0,
		fetching: false,
		isOpen: false,
	};

	componentWillMount() {
		const placeholder = this.props.placeholder || 'Search';
		this.setState({ placeholder });
	};

	onKeyDown = (e: any) => {
		const keyCode = e.keyCode;
		if (e.keyCode === 13 && this.state.activeSuggestion > 0) {
			this.setState({
				activeSuggestion: 0,
				isOpen: false,
				search: this.state.suggestions[this.state.activeSuggestion - 1].name,
			});
		} else if (keyCode === 38) {
			const atStart = this.state.activeSuggestion === 0;
			if (atStart) return;
			this.setState({ activeSuggestion: this.state.activeSuggestion - 1 })
		} else if (keyCode === 40) {
			const atEnd = this.state.activeSuggestion === this.state.suggestions.length;
			if (atEnd) return;
			this.setState({ activeSuggestion: this.state.activeSuggestion + 1 })
		}
	}

	setOpeness = (isOpen: boolean) => {
		this.setState({ isOpen });
	}

	onFocus = () => {
		this.setOpeness(true);
	}

	onChange = (e: any) => {
		e.preventDefault();
		const value = e.currentTarget.value;
		const shouldSendRequest = value.length > 0;
		const suggestions = shouldSendRequest ? this.state.suggestions : [];
		this.setState({
			search: value,
			fetching: shouldSendRequest,
			suggestions,
		}, () => {
			if (shouldSendRequest) {
				this.searchSuggestionDebounce();
			};
		});
	};

	searchSuggestionDebounce = () => {
		const duration = 500;

		clearTimeout(this.timer);

		this.timer = setTimeout(() => {
			this.sendRequest();
		}, duration);
	}

	onClick = (e: any) => {
		e.preventDefault();

		const value = e.currentTarget.getAttribute('data-value');

		const search = `?q=${value}`;

		this.setState({ results: [], activeSuggestion: 0, isOpen: false, search: value });

		if (this.props.hideSearchBarDisplay) {
			this.props.hideSearchBarDisplay();
		}

		this.props.history.push(`/search${search}`);
		this.props.location.pathname = `/search${search}`;
		this.props.location.search = search;
	};

	onSubmit = (e: any) => {
		e.preventDefault();

		if (this.state.search.length === 0) return;

		const search = `?q=${this.state.search}`;

		this.setState({ results: [], activeSuggestion: 0, isOpen: false });

		if (this.props.hideSearchBarDisplay) {
			this.props.hideSearchBarDisplay();
		}

		this.props.history.push(`/search${search}`);
		this.props.location.pathname = `/search${search}`;
		this.props.location.search = search;
	};

	sendRequest = () => {
		fetch(`${API_URL}/search/suggestions?q=${this.state.search}`)
		.then(res => {
			if (!res.ok) throw res;
			return res.json();
		})
		.then(response => this.setState({ suggestions: response, fetching: false, isOpen: response.length > 0, activeSuggestion: 0 }))
		.catch(error => console.error(error));
	};

	render() {
		const renderSuggestions = this.state.isOpen;
		return (
			<div id="search-bar" ref={this.searchBar}>
				<form onSubmit={this.onSubmit}>
					<input
						placeholder={this.state.placeholder}
						type="text"
						onChange={this.onChange}
						onKeyDown={this.onKeyDown}
						value={this.state.search}
						onFocus={this.onFocus}
						ref={this.input}
					/>
					<SearchButton fetching={this.state.fetching} />
				</form>
				{renderSuggestions &&
					<Suggestions
						setOpeness={this.setOpeness}
						isOpen={this.state.isOpen}
						suggestions={this.state.suggestions}
						inputRef={this.input}
						activeSuggestion={this.state.activeSuggestion}
						onClick={this.onClick}
					/>
				}
			</div>
		)
	}
};

const SearchButton = (props: any) => {
	const { fetching } = props;
	const renderButtonOrLoadingIcon = fetching
	? (
		<span className="loading-spinner">
			<i className="fas fa-spinner fa-spin" />
		</span>
	)
	: (
		<button>
			<i className="fas fa-search" />
		</button>
	);

	return renderButtonOrLoadingIcon;
}

class Suggestions extends Component<any> {
	private container: any;
	constructor(props: any) {
		super(props);
		this.container = React.createRef();
	}

	componentDidMount() {
		this.props.setOpeness(true);
		window.addEventListener('click', this.onClickOutsideHandler);
	}

	componentWillUnmount() {
		window.removeEventListener('click', this.onClickOutsideHandler);
	}

	onClickOutsideHandler = (e: Event) => {
		if (this.props.isOpen
			&& !this.container.current.contains(e.target)
			&& !this.props.inputRef.current.contains(e.target)) {
			this.props.setOpeness(false);
		}
	}

	render() {
		const activeSuggestion = this.props.activeSuggestion;
		const renderSuggestions = this.props.suggestions.map((result: any, index: number) => {
			return <Suggestion
				key={index}
				label={result.name}
				active={index + 1 === activeSuggestion}
				onClick={this.props.onClick}
			/>
		});

		return (
			<div className="search-suggestions" ref={this.container}>
				{this.props.isOpen && renderSuggestions}
			</div>
		);
	}
};

const Suggestion = (props: any) => {
	return (
		<div
			onClick={props.onClick}
			data-value={props.label}
			data-focus={props.active.toString()}
			className="search-suggestion"
		>
			{props.label}
		</div>
	)
};

export default withRouter(SearchBar);