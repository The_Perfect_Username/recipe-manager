import React, { Component } from 'react';
import { userLoggedIn, logout } from '../../utils/Auth';
import { Link } from 'react-router-dom';
import SearchBar from './Search/SearchBar';
import Dropdown from '../Layouts/Dropdown';
import BlueLogo from '../../Images/logo.svg';

class Header extends Component {
	private searchBar: any;
	private searchBarButton: any;

	constructor(props: any) {
		super(props);
		this.searchBar = React.createRef();
		this.searchBarButton = React.createRef();
	}

	state = {
		showSearchBar: false,
	}

	componentDidMount() {
		window.addEventListener('click', this.onClickOutsideHandler);
	}

	toggleSearchBarDisplay = () => {
		this.setState({
			showSearchBar: !this.state.showSearchBar
		});
	}

	hideSearchBarDisplay = () => {
		this.setState({ showSearchBar: false });
	}

	onClickOutsideHandler = (e: Event) => {
		if (this.state.showSearchBar
			&& !this.searchBar.current.contains(e.target)
			&& !this.searchBarButton.current.contains(e.target)) {
			this.hideSearchBarDisplay();
		}
	}

	render() {
		const isLoggedIn = userLoggedIn();
		const user: any = window.localStorage.getItem('user') || '{}';
		const name = JSON.parse(user).name || null;

		const renderElement = isLoggedIn
			? <DropDownAccount name={name} />
			: (
				<>
					<Link className="entry-link" to='/signup'>Register</Link>
					<Link className="entry-link btn" to='/signin'>Login</Link>
				</>
			);

		return (
			<header>
				<div className="container">
					<SiteLogo />
					<div className="search-bar-container hide-mobile">
						<SearchBar />
					</div>
					<HeaderControls
						isLoggedIn={isLoggedIn}
						renderElement={renderElement}
						toggleSearchBarDisplay={this.toggleSearchBarDisplay}
						searchBarButton={this.searchBarButton}
					/>
				</div>
				{this.state.showSearchBar &&
					<div className="search-bar-container mobile show-mobile" ref={this.searchBar}>
						<SearchBar hideSearchBarDisplay={this.hideSearchBarDisplay} />
					</div>
				}
			</header>
		);
	}
}

const SiteLogo = () => (
	<div id="logo">
		<Link to="/">
			<img
				className="logo"
				src={BlueLogo}
				alt="logo"
				height={40}
			/>
		</Link>
	</div>
);

const HeaderControls = (props: any) => {
	const {
		isLoggedIn,
		renderElement,
		toggleSearchBarDisplay,
		searchBarButton,
	} = props;

	return (
		<div className="header-controls">
			<div
				ref={searchBarButton}
				onClick={toggleSearchBarDisplay}
				className="mobile-search-btn show-mobile"
			>
				<i className="fas fa-search" />
			</div>
			{isLoggedIn &&
				<Link className="create-btn" to='/post/create'>
					<i className="far fa-paper-plane" />
					<span>Create</span>
				</Link>
			}
			{renderElement}
		</div>
	);
}

const DropDownAccount = (props: any) => {
	const { name } = props;
	let dropdownItems = [
        {
            label: 'Account',
			pathname: '/account',
			data: {
				account_id: null,
			}
        },
        {
            label: 'Create',
			pathname: '/post/create',
			className: ['mobile-only'],
        },
        {
            label: 'Logout',
			action: () => logout()
        },
    ];
	return (
		<Dropdown menu={dropdownItems}>
			<a className="header-btn spaced" href="#">
				<span>{name}</span>
				<i className="fas fa-sort-down" />
			</a>
		</Dropdown>
	)

}

export default Header;