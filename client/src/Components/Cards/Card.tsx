import React, { Component } from 'react';
import './Card.scss';
import { Link } from 'react-router-dom';
import { API_URL } from '../Form/Form';

const moment = require('moment');

export class Card extends Component<any, any> {

    state = {
        loading: true,
    }

    render() {

        const {
            id,
            title,
            filename,
            author,
            rating,
            revisionParent,
            created,
        } = this.props;

        const url = `${API_URL}/image/thumbnail/${filename}`;

        const renderTile = this.state.loading
            ? <Tile />
            : null;

        const mDate = moment(created);
        const timeAgo = moment(mDate).fromNow();

        return (
            <div className="card">
                <div className="card-shadow"></div>
                <Link to={`/post/${id}`}>
                    <div className="card-image-container">
                        {renderTile}
                        <img onLoad={() => this.setState({ loading: false })} src={url} alt={title} />
                    </div>
                    <CardContent
                        title={title}
                        author={author}
                        created={timeAgo}
                        rating={rating}
                        revisionParent={revisionParent}
                    />
                </Link>
            </div>
        );
    }
}

const CardContent = (props: any) => {
    const {
        title,
        author,
        created,
        rating,
        revisionParent,
    } = props;

    const averageRating = rating ? rating.toFixed(1) : null;

    return (
        <div className="card-content">
            <span className="title">{title}</span>
            <span className="author">{author}</span>
            <div className="card-meta">
                <div className="flex-start">
                    <span className="average-rating">
                        <i className="rating-btn far fa-star" />
                        <span>{averageRating}</span>
                    </span>
                    {revisionParent &&
                        <span className="average-rating" title="Revision">
                            <i className="far fa-clone" />
                        </span>
                    }
                </div>
                <span className="date">{created}</span>
            </div>
        </div>
    )
};

export const CardsContainer: React.FC = () => {
    return (
        <div className="card-grid"></div>
    );
}

export const Tile = () => <div className="card-tile"></div>

export const LoadingCard = () => (
    <div className="card preloading">
        <div className="image-placeholder">
            <Tile />
        </div>
        <div className="content-placeholder">
            <span>
                <Tile />
            </span>
            <span className="short">
                <Tile />
            </span>
        </div>
    </div>
)
