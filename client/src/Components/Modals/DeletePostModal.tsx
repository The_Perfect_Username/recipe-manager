import React, { Component } from 'react';
import Modal from 'react-modal';
import { CLIENT_URL, API_URL } from '../Form/Form';

Modal.setAppElement('#root');

class DeletePostModal extends Component<any> {
    state = {
        id: '',
    }

    componentWillMount() {
        this.setState({
            id: this.props.recipeId,
        });
    }

    componentWillReceiveProps(props: any) {
        this.setState({
            id: props.recipeId,
        });
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        const id = this.state.id;

        fetch(`${API_URL}/recipe/${id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            }
        })
        .then(res => {
            if (!res.ok) throw res;
            window.location.replace(`${CLIENT_URL}/account`);
        })
        .catch(error => {
            console.error(error);
        });
    }

    render() {
        const {
            showModal,
            handleCloseModal
        } = this.props;

        return (
            <Modal
                isOpen={showModal}
                onRequestClose={handleCloseModal}
                className="modal"
                overlayClassName="modal-overlay"
            >
                <div className="modal-body">
                    <span>Are you sure you want to delete your recipe? This cannot be undone.</span>
                </div>
                <div className="modal-footer reversed">
                    <button onClick={this.onSubmit} className="btn-danger">Delete</button>
                    <button onClick={handleCloseModal} className="btn-link">Cancel</button>
                </div>
            </Modal>
        )
    }
}

export default DeletePostModal;