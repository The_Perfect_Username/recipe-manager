import React from 'react';

const ErrorContainer: React.FC = (props: any) => (
    <div className="error-page">
        <h2>An error occurred!</h2>
        <p>
            {props.children}
        </p>
    </div>
)

export default ErrorContainer;