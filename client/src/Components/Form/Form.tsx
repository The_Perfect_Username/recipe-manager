import React from 'react';
import './Form.scss';

const protocol = window.location.protocol;

export const API_URL = process.env.NODE_ENV === "production"
    ? `${protocol}//api.opensaucepan.com`
    : `${protocol}//localhost:3001`;

export const CLIENT_URL = process.env.NODE_ENV === "production"
    ? `${protocol}//www.opensaucepan.com`
    : `${protocol}//localhost:3000`;

export const processText = (string: string, type: string) => {
    const lines = string.split('\n').filter(line => line.length > 0);
    if (type === 'ul') {
        return <ul>{lines.map((line, index) => <li key={index}>{line}</li>)}</ul>;
    } else if (type === 'ol') {
        return <ol>{lines.map((line, index) => <li key={index}>{line}</li>)}</ol>;
    } else {
        return lines.map((line, index) => <p key={index}>{line}</p>);
    }
}

export const PreviewText: React.FC<IPreviewText> = (props) => (
    <div className="preview-field">
        {processText(props.value, props.type)}
    </div>
)

export const FormGroup: React.FC<IFormGroup> = (props) => {
    const { children, className } = props;
    let classes = className ? `form-group ${className}` : 'form-group';
    return (
        <div className={classes}>
            {children}
        </div>
    )
}

export const Label: React.FC = (props) => {
    return (
        <div className="form-label-container">
            <label>{props.children}</label>
        </div>
    )
}

export const ErrorMessage: React.FC = (props) => {
    const {
        children,
    } = props;
    return (
        <div className="error-message">
            <span>{children}</span>
        </div>
    )
}

export const LabelWithCounter: React.FC<any> = (props) => {
    const {
        label,
        counter,
    } = props;

    return (
        <div className="spaced">
            <Label>{label}</Label>
            <span className='counter'>{counter}</span>
        </div>
    )
}

interface IFormGroup {
    className?: string;
}

interface IPreviewText {
    value: string;
    type: string;
}