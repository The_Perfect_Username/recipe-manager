import React, { Component } from 'react';
import { Card, LoadingCard } from '../Cards/Card';
import { API_URL, CLIENT_URL } from '../Form/Form';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import './Results.scss';

class ResultsGrid extends Component<
    RouteComponentProps<any>,
    {
        error: boolean,
        loading: boolean,
        hasMore: boolean,
        page: number,
        results: any[]
    }
> {
    constructor(props: any) {
        super(props);
        this.state = {
            error: false,
            loading: false,
            hasMore: true,
            page: 0,
            results: [],
        }
        window.onscroll = () => this.onScroll();
    }

    componentDidMount() {
        const pathname = this.props.location.pathname || '';

        if (pathname === '/search') {
            const query = this.props.location.search;
            if (query.length <= 3 || !query.match(/^\?q=/g)) {
                window.location.replace(CLIENT_URL);
            }
        }

        this.sendRequest();
    }

    componentWillReceiveProps() {
        const pathname = this.props.location.pathname || '';

        if (pathname === '/search') {
            const query = this.props.location.search;
            if (query.length <= 3 || !query.match(/^\?q=/g)) {
                window.location.replace(CLIENT_URL);
            }
        }

        this.sendRequest();
    }

    sendRequest = () => {
        const search = this.props.location.search;
        const query = search.length > 0 ? search : '';

        this.setState({ loading: true });

        fetch(`${API_URL}/search${query}`)
            .then(res => {
                if (!res.ok) throw res;
                return res.json();
            })
            .then(response => {
                const numberOfExpectedResults = 12;
                const results = response || [];
                const nextPage = this.state.page + 1;
                const hasMore = results.length === numberOfExpectedResults;
                this.setState({
                    hasMore: hasMore,
                    page: nextPage,
                    results: results,
                    loading: false,
                });
            })
            .catch(error => {
                console.error(error);
                this.setState({
                    error: true,
                });
            });
    }

    onLoadMore = () => {
        const search = this.props.location.search;
        const query = search.length > 0 ? `${search}&page=${this.state.page}` : `page=${this.state.page}`;

        this.setState({ loading: true });

        fetch(`${API_URL}/search?${query}`)
            .then(res => {
                if (!res.ok) throw res;
                return res.json();
            })
            .then(response => {
                const numberOfExpectedResults = 12;
                const results = response || [];
                const nextPage = this.state.page + 1;
                const hasMore = results.length === numberOfExpectedResults;

                this.setState(prevState => ({
                    loading: false,
                    error: false,
                    hasMore: hasMore,
                    page: nextPage,
                    results: [...prevState.results, ...results],
                }));
            })
            .catch(error => {
                console.error(error);
                this.setState({
                    error: true,
                });
            })
    }

    onScroll = () => {
        const {
            error,
            loading,
            hasMore,
        } = this.state;

        if (error || loading || !hasMore) return;

        const docElement = document.documentElement;
        const scrollHeight = docElement.scrollHeight;
        const scrollTop = docElement.scrollTop;
        const clientTop = docElement.clientHeight;

        const difference = scrollHeight - scrollTop;

        if (difference <= (clientTop + 20)) {
            this.onLoadMore();
        }
    }

    render() {

        const results = this.state.results.map((recipe, i) => (
            <Card
                key={i}
                id={recipe.id}
                title={recipe.name}
                filename={recipe.filename}
                author={recipe.display_name || recipe.user_name}
                rating={recipe.rating}
                revisionParent={recipe.revision_parent}
                created={recipe.created}
            />
        ));

        const renderElements = !this.state.loading && results.length === 0
            ? <NoResults />
            : results;

        return (
            <div className="results">
                <CardGrid>
                    {renderElements}
                    {this.state.loading && <LoadingGroup />}
                </CardGrid>
            </div>
        );
    }
}

export const LoadingGroup = () => {
    let cards = [];
    for (let i = 0; i < 6; i++) {
        cards.push(<LoadingCard key={i} />);
    }
    return <>{cards}</>;
}

export const CardGrid: React.FC = (props) => (
    <div className="card-grid">
        {props.children}
    </div>
)

export const NoResults = () => (
    <div className="no-results">
        <h3>No results found</h3>
    </div>
)

export default withRouter(ResultsGrid);