import React, { Component } from 'react';
import ResultsGrid from './ResultsGrid';
import { Link } from 'react-router-dom';
import { userLoggedIn, logout } from '../../utils/Auth';
import { handleError } from '../../utils/ErrorHandling';
import { API_URL } from '../Form/Form';
import './Account.scss';

const moment = require('moment');

export default class Account extends Component<any> {
    state = {
        account_id: null,
        belongs_to_user: false,
        type: '',
        user_name: '',
        display_name: '',
        created: '',
        loaded: false,
        type_count: {
            all: 0,
            original: 0,
            revision: 0,
        },
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        const match = this.props.match || null;
        const history = this.props.history || null;
        const user = window.localStorage.getItem('user');
        const id = match && match.params && match.params.id;

        if (id && history) {
            const belongsToUser = user && JSON.parse(user).id === id;
            if (belongsToUser) {
                return history.push('/account');
            }

            this.setState({
                account_id: id,
                belongs_to_user: belongsToUser,
            }, () => {
                this.fetchDetails()
            });

        } else {
            this.setState({
                belongs_to_user: true,
                account_id: null,
            }, () => {
                this.fetchDetails()
            });
        }
    }

    componentWillReceiveProps(props: any) {
        const match = props.match || null;
        const history = props.history || null;
        const user = window.localStorage.getItem('user');
        const id = match && match.params && match.params.id;

        if (id && history) {
            const belongsToUser = user && JSON.parse(user).id === id;

            if (belongsToUser) {
                return history.push('/account');
            }

            this.setState({
                account_id: id,
                belongs_to_user: belongsToUser
            }, () => {
                this.fetchDetails();
            });

        } else {
            this.setState({
                belongs_to_user: true,
                account_id: null,
            }, () => {
                this.fetchDetails();
            });
        }
    }

    fetchDetails = () => {
        const userIsLoggedIn = userLoggedIn();
        const fetchParams: any = {};

        let query = '';

        if (userIsLoggedIn) {
            fetchParams.headers = {
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            }
        }

        if (this.state.account_id) {
            query = `?account_id=${this.state.account_id}`;
        }

        fetch(`${API_URL}/account${query}`, fetchParams)
        .then(res => {
            if (!res.ok) throw res;
            return res.json();
        })
        .then(response => {
            this.setState({ ...this.state, ...response, loaded: true });
        })
        .catch(error => {
            handleError(error)
            .then(error => {
                if (error.is_banned) {
                    logout();
                }
            })
        });
    }

    changeTab = (type: string) => {
        this.setState({
            type,
        });
    }

    render() {

        const mDate = moment(this.state.created);
        const timeAgo = moment(mDate).fromNow();

        const showEditLink = this.state.loaded && this.state.belongs_to_user;

        return (
            <div className="account-container">
                <div className="account-details">
                    <div>
                        <span className="username">
                            {this.state.display_name || this.state.user_name}
                        </span>
                        {this.state.loaded &&
                            <span className="date">Joined {timeAgo}</span>
                        }
                    </div>
                    {showEditLink &&
                        <Link className="std-link" to="/account/edit">Edit</Link>
                    }
                </div>
                <div className="account-header">
                    <AccountNav
                        changeTab={this.changeTab}
                        belongsToUser={this.state.belongs_to_user}
                        accountId={this.state.account_id}
                        typeCount={this.state.type_count}
                    />
                </div>
                <div className="account-content">
                    <ResultsGrid
                        type={this.state.type}
                        accountId={this.state.account_id}
                        loaded={this.state.loaded}
                    />
                </div>
            </div>
        )
    }
}

class AccountNav extends Component<any> {
    state = {
        type_count: {
            all: 0,
            original: 0,
            revision: 0,
        },
    };

    componentWillMount() {
        this.setState({ type_count: { ...this.props.typeCount } });
    }
    componentWillReceiveProps(props: any) {
        this.setState({ type_count: { ...props.typeCount } });
    }

    onClick = (e: any) => {
        e.preventDefault();

        const allLinks = document.querySelectorAll('#account-nav a');
        const target = e.currentTarget;
        const type = target.getAttribute('rel') || '';

        this.props.changeTab(type);

        allLinks.forEach((link) => {
            link.classList.remove('active');
        });

        target.classList.add('active');
    }

    render() {
        return (
            <nav id="account-nav">
                <a className="active" onClick={this.onClick}>
                    <span className="post-label">
                        All Posts
                    </span>
                    <span className="post-count">
                        {this.state.type_count.all}
                    </span>
                </a>
                <a rel="original" onClick={this.onClick}>
                    <span className="post-label">
                        Created
                    </span>
                    <span className="post-count">
                    {this.state.type_count.original}
                    </span>
                </a>
                <a rel="revision" onClick={this.onClick}>
                    <span className="post-label">
                        Revisions
                    </span>
                    <span className="post-count">
                        {this.state.type_count.revision}
                    </span>
                </a>
                {this.props.belongsToUser &&
                    <>
                        <a rel="rated" onClick={this.onClick}>
                            <span className="post-label">
                                Rated
                            </span>
                            <span className="post-count">
                                <i className="far fa-star" />
                            </span>
                        </a>
                        <a rel="saved" onClick={this.onClick}>
                            <span className="post-label">
                                Saved
                            </span>
                            <span className="post-count">
                                <i className="far fa-bookmark" />
                            </span>
                        </a>
                    </>
                }
            </nav>
        )
    }
}
