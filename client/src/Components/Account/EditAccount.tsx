import React, { Component } from 'react';
import { FormGroup, Label, API_URL, ErrorMessage } from '../Form/Form';
import { handleError } from '../../utils/ErrorHandling';
import DeleteAccountForm from './DeleteAccountForm';

const EditAccount: React.FC = () => (
    <>
        <div id="edit-account" className="form-container">
            <EditAccountForm />
            <PasswordChangeForm />
        </div>
        <div id="delete-account" className="form-container">
            <DeleteAccountForm />
        </div>
    </>
);

class EditAccountForm extends Component {

    state = {
        name: '',
        display_name: '',
        email: '',
        name_error: '',
        email_error: '',
        display_name_error: '',
        account_error: '',
        error: '',
        disable_account_button: false,
    }

    componentDidMount() {
        const user: any = window.localStorage.getItem('user');
        const userObject = JSON.parse(user);
        this.setState({
            ...userObject,
        });
    }

    formHasErrors = () => {
        const nameErrorLength = this.state.name_error.length;
        const displayNameErrorLength = this.state.display_name_error.length;
        const emailErrorLength = this.state.email_error.length;

        return (nameErrorLength + displayNameErrorLength + emailErrorLength) > 0;
    }

    handleNameInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = this.validateName(value);

        this.setState({
            name: value,
            name_error: error,
        });
    }

    handleDisplayNameInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = this.validateDisplayName(value);

        this.setState({
            display_name: value,
            display_name_error: error,
        });
    }

    validateName = (value: string) => {
        let error = '';

        if (value.length === 0 || value.trim().length === 0) {
            error = 'Name must not be left empty or blank';
        }

        if (value.length > 50) {
            error = 'Name must not exceed 50 characters';
        }

        return error;
    }

    validateDisplayName = (value: string) => {
        let error = '';

        if (value && !value.match(/^[A-Za-z0-9_-]+$/)) {
            error = 'Display name must only contain alphanumeric characters and "-" and "_"';
        }

        if (value.length > 15) {
            error = 'Display name must not exceed 15 characters';
        }

        return error;
    }

    handleEmailInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = this.validateEmail(value);

        this.setState({
            email: value,
            email_error: error,
        });
    }

    validateEmail = (value: string) => {
        let error = '';

        if (value.length === 0 || value.trim().length === 0) {
            error = 'Email must not be left empty or blank';
        }

        if (value.length > 100) {
            error = 'Email must not exceed 100 characters';
        }

        if (value.indexOf('@') === -1) {
            error = 'Invalid email address';
        }

        return error;
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        const hasErrors = this.formHasErrors();

        if (hasErrors) return;

        this.setState({ disable_account_button: true });

        fetch(`${API_URL}/user`, {
            method: 'put',
            body: JSON.stringify({
                name: this.state.name,
                display_name: this.state.display_name,
                email: this.state.email,
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            },
        })
        .then(res => {
            if (!res.ok) throw res;
            return res;
        })
        .then(_ => {
            window.localStorage.setItem('user', JSON.stringify({
                name: this.state.name,
                display_name: this.state.display_name,
                email: this.state.email,
            }));

            this.setState({
                disable_account_button: false,
                error: '',
                account_error: '',
            });
        })
        .catch(error => {
            handleError(error)
            .then(error => {
                const message = 'message' in error ? error.message : '';
                this.setState({
                    error: message,
                    ...error,
                    disable_account_button: false,
                });
            });
        });
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <FormGroup>
                    <Label>Name</Label>
                    <input
                        className="form-control"
                        value={this.state.name}
                        onChange={this.handleNameInput}
                    />
                    {this.state.name_error.length > 0 &&
                        <ErrorMessage>{this.state.name_error}</ErrorMessage>
                    }
                </FormGroup>
                <FormGroup>
                    <Label>Display Name (Optional)</Label>
                    <input
                        className="form-control"
                        value={this.state.display_name}
                        onChange={this.handleDisplayNameInput}
                    />
                    {this.state.display_name_error.length > 0 &&
                        <ErrorMessage>{this.state.display_name_error}</ErrorMessage>
                    }
                </FormGroup>
                <FormGroup>
                    <Label>Email</Label>
                    <input
                        className="form-control"
                        value={this.state.email}
                        onChange={this.handleEmailInput}
                    />
                    {this.state.email_error.length > 0 &&
                        <ErrorMessage>{this.state.email_error}</ErrorMessage>
                    }
                </FormGroup>
                {this.state.account_error.length > 0 &&
                    <><ErrorMessage>{this.state.account_error}</ErrorMessage><br /></>
                }
                {this.state.error.length > 0 &&
                    <><ErrorMessage>{this.state.error}</ErrorMessage><br /></>
                }
                <FormGroup className="reversed">
                    <button disabled={this.state.disable_account_button} className="btn-submit">
                        {this.state.disable_account_button &&
                            <span className="loading-spinner">
                                <i className="fas fa-spinner fa-spin" />
                            </span>
                        }
                        Save
                    </button>
                </FormGroup>
            </form>
        )
    }
}

class PasswordChangeForm extends Component {

    state = {
        password: '',
        show_password_form: false,
        password_error: '',
        error: '',
        disable_password_button: false,
    }

    passwordFormHasErrors = () => {
        const passwordErrorLength = this.state.password_error.length;

        return passwordErrorLength > 0;
    }

    onPasswordSubmit = (e: any) => {
        e.preventDefault();

        const hasErrors = this.passwordFormHasErrors();

        if (hasErrors) return;

        this.setState({ disable_password_button: true });

        fetch(`${API_URL}/user/password`, {
            method: 'put',
            body: JSON.stringify({
                password: this.state.password,
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            },
        })
        .then(res => {
            if (!res.ok) throw res;

            return res;
        })
        .then(_ => {
            this.setState({
                show_password_form: !this.state.show_password_form,
                disable_password_button: false,
                error: '',
            });
        })
        .catch(error => {
            handleError(error)
            .then(error => {

                if (typeof error === 'string') {

                    this.setState({
                        error,
                        disable_password_button: false,
                    });

                } else if (typeof error === 'object') {
                    const message = 'message' in error ? error.message : '';
                    this.setState({
                        error: message,
                        ...error,
                        disable_password_button: false,
                    });
                }
            });
        });
    }

    handlePasswordInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = this.validatePassword(value);
        this.setState({
            password: value,
            password_error: error,
        });
    }

    validatePassword = (value: string) => {
        let error = '';

        if (value.length < 6) {
            error = 'Password must have at least 6 characters';
        }

        if (value.indexOf(' ') >= 0) {
            error = 'Password must not contain whitespace';
        }

        return error;
    }

    togglePasswordForm = (e: any) => {
        e.preventDefault();
        this.setState({
            show_password_form: !this.state.show_password_form,
        });
    }

    cancelPasswordChange = (e: any) => {
        e.preventDefault();
        this.setState({
            password: '',
            show_password_form: false,
            password_error: '',
        });
    }

    render() {
        return (
            <form onSubmit={this.onPasswordSubmit}>
                {(this.state.show_password_form && (
                    <>
                        <FormGroup>
                            <Label>Change Password</Label>
                            <input
                                className="form-control"
                                type="password"
                                value={this.state.password}
                                onChange={this.handlePasswordInput}
                            />
                            {this.state.password_error.length > 0 &&
                                <ErrorMessage>{this.state.password_error}</ErrorMessage>
                            }
                        </FormGroup>
                        <FormGroup className="reversed">
                            <button disabled={this.state.disable_password_button} className="btn-danger">
                                {this.state.disable_password_button &&
                                    <span className="loading-spinner">
                                        <i className="fas fa-spinner fa-spin" />
                                    </span>
                                }
                                Change Password
                            </button>
                            <button className="btn-link" onClick={this.cancelPasswordChange}>Cancel</button>
                        </FormGroup>
                    </>
                    )) || (
                        <FormGroup>
                            <Label>Change Password</Label>
                            <input
                                className="form-control editable-mask"
                                type="password"
                                placeholder="••••••••"
                                onClick={this.togglePasswordForm}
                            />
                        </FormGroup>
                    )
                }
                {this.state.error.length > 0 &&
                    <ErrorMessage>{this.state.error}</ErrorMessage>
                }
            </form>
        )
    }
}

export default EditAccount;