import React, { Component } from 'react';
import { Card } from '../../Components/Cards/Card';
import {
    CardGrid,
    NoResults,
    LoadingGroup,
} from '../../Components/Results/ResultsGrid';
import { API_URL } from '../Form/Form';
import { userLoggedIn } from '../../utils/Auth';

const userIsLoggedIn = userLoggedIn();

export default class ResultsGrid extends Component<
    {
        type: string,
        accountId: string | null;
        loaded: boolean;
    },
    {
        type: string,
        error: boolean,
        loading: boolean,
        has_more: boolean,
        offset: number,
        results: any[]
    }
> {
    constructor(props: any) {
        super(props);
        this.state = {
            type: '',
            error: false,
            loading: false,
            has_more: true,
            offset: 0,
            results: [],
        }
        window.onscroll = () => this.onScroll();
    }

    componentWillReceiveProps(props: any) {
        const type = props.type || '';

        this.setState({
            type,
            offset: 0,
            results: [],
        }, () => {
            if (props.loaded) {
                this.sendRequest();
            }
        });
    }

    onLoadMore = () => {
        this.sendRequest();
    }

    sendRequest = () => {
        const type = this.state.type;

        const isSavedOrRated = (type === 'saved' || type === 'rated')

        let url = isSavedOrRated
        ? `${API_URL}/account/${type}`
        : `${API_URL}/account/recipes`;

        const limit = 12;
        const offset = this.state.offset;

        url = this.state.type.length > 0
            ? `${url}?type=${this.state.type}&limit=${limit}&offset=${offset}`
            : `${url}?limit=${limit}&offset=${offset}`;

        url = (this.props.accountId && !isSavedOrRated)
            ? `${url}&account_id=${this.props.accountId}`
            : url;

        this.setState({ loading: true });

        const fetchParams: any = {};

        if (!this.props.accountId && userIsLoggedIn) {
            fetchParams.headers = {
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            }
        }

        fetch(url, fetchParams)
        .then(res => {
            if (!res.ok) throw res;
            return res.json();
        })
        .then(response => {
            const newOffset = limit + offset;
            this.setState(prevState => ({
                loading: false,
                error: false,
                has_more: response.length === limit,
                offset: newOffset,
                results: [...prevState.results, ...response],
            }));
        })
        .catch(error => {
            console.error(error);
            this.setState({
                error: true,
            });
        })
    }

    onScroll = () => {
        const {
            error,
            loading,
            has_more,
        } = this.state;

        if (error || loading || !has_more) return;

        const docElement = document.documentElement;
        const scrollHeight = docElement.scrollHeight;
        const scrollTop = docElement.scrollTop;
        const clientTop = docElement.clientHeight;

        const difference = scrollHeight - scrollTop;

        if (difference <= (clientTop + 20)) {
            this.onLoadMore();
        }
    }

    render() {

        const results = this.state.results.map((recipe, i) => (
            <Card
                key={i}
                id={recipe.id}
                title={recipe.name}
                filename={recipe.filename}
                author={recipe.display_name || recipe.user_name}
                rating={recipe.rating}
                revisionParent={recipe.revision_parent}
                created={recipe.created}
            />
        ));

        const renderElements = !this.state.loading && results.length === 0
            ? <NoResults />
            : results;

        return (
            <div className="results">
                <CardGrid>
                    {renderElements}
                    {this.state.loading && <LoadingGroup />}
                </CardGrid>
            </div>
        );
    }
}
