import React, { Component } from 'react';
import { FormGroup, Label, API_URL, ErrorMessage } from '../Form/Form';
import { handleError } from '../../utils/ErrorHandling';
import { logout } from '../../utils/Auth';

export default class DeleteAccountForm extends Component {
    state = {
        password: '',
        error: '',
        show_form: false,
        disable_button: false,
    }

    toggleShowForm = (e: any) => {
        e.preventDefault();
        this.setState({
            show_form: !this.state.show_form,
        })
    }

    onChange = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;

        this.setState({
            password: value,
        });
    }

    cancel = (e: any) => {
        e.preventDefault();
        this.setState({
            password: '',
            error: '',
            show_form: false,
        });
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        fetch(`${API_URL}/account`, {
            method: 'DELETE',
            body: JSON.stringify({
                password: this.state.password,
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            },
        })
        .then(res => {
            if (!res.ok) throw res;

            return res.json();
        })
        .then(_ => {
            logout();
        })
        .catch(error => {
            handleError(error)
            .then(error => {
                this.setState({
                    ...error,
                    disable_button: false,
                });
            });
        });
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                {this.state.show_form && (
                    <ActiveForm
                        password={this.state.password}
                        onChange={this.onChange}
                        cancel={this.cancel}
                        disableButton={this.state.disable_button}
                        error={this.state.error}
                    />
                )}
                {!this.state.show_form && (
                    <PromptForm showForm={this.toggleShowForm} />
                )}

            </form>
        )
    }
}

const PromptForm: React.FC<any> = (props: any) => {
    const {
        showForm,
    } = props;
    return (
        <FormGroup>
            <Label>Delete my account</Label>
            <button
                className="btn-danger"
                onClick={showForm}
            >
                Proceed to Delete Account
            </button>
        </FormGroup>
    )
}

const ActiveForm: React.FC<any> = (props: any) => {
    const {
        password,
        onChange,
        cancel,
        disableButton,
        error,
    } = props;

    return (
        <>
            <FormGroup>
                <Label>Enter your Password</Label>
                <span className="form-info">
                    Are you sure you want to delete your account? This cannot be undone.
                </span>
                <input
                    className="form-control"
                    value={password}
                    onChange={onChange}
                    type="password"
                    autoComplete="new-password"
                />
                {error && <ErrorMessage>{error}</ErrorMessage>}
            </FormGroup>
            <FormGroup className="reversed">
                <button disabled={disableButton} className="btn-danger">Delete Account</button>
                <button className="btn-link" onClick={cancel}>Cancel</button>
            </FormGroup>
        </>
    )
}
