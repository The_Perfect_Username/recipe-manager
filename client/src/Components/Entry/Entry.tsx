import React, { Component } from 'react';
import { Label, FormGroup, ErrorMessage, API_URL, CLIENT_URL } from '../Form/Form';
import { Link } from 'react-router-dom';
import { handleError } from '../../utils/ErrorHandling';
import './Form.scss';

const EntryForm: React.FC = (props) => {
    return (
        <div className="form-container condensed">
            {props.children}
        </div>
    )
}
const FormHeader: React.FC = (props: any) => (
    <div className="form-header">
        {props.children}
    </div>
)
export class RegistrationForm extends Component {

    state = {
        name: '',
        email: '',
        password: '',
        name_error: '',
        email_error: '',
        password_error: '',
        error: '',
        disable_button: false,
    }

    disableButton = (disable: boolean) => {
        this.setState({ disable_button: disable });
    }

    formHasErrors = () => {
        const nameErrorLength = this.state.name_error.length;
        const emailErrorLength = this.state.email_error.length;
        const passwordErrorLength = this.state.password_error.length;

        return (nameErrorLength + emailErrorLength + passwordErrorLength) > 0;
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        const formHasErrors = this.formHasErrors();

        if (formHasErrors) return;

        this.disableButton(true);

        fetch(`${API_URL}/user`, {
            method: 'post',
            body: JSON.stringify({
                name: this.state.name,
                email: this.state.email,
                password: this.state.password,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(res => {
            if (!res.ok) throw res;
            return res.json();
        })
        .then(response => {
            const localStorage = window.localStorage;
            localStorage.setItem('token', response.token);
            localStorage.setItem('user', JSON.stringify(response.user));
            window.location.replace(`${CLIENT_URL}/account`);
        })
        .catch(error => {
            handleError(error)
            .then(error => {
                this.setState({
                    ...error,
                    disable_button: false,
                });
            });
        });
    }

    handleNameInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = this.validateName(value);

        this.setState({
            name: value,
            name_error: error,
        });

    }

    validateName = (value: string) => {
        let error = '';

        if (value.length === 0 || value.trim().length === 0) {
            error = 'Name must not be left empty or blank';
        }

        if (value.length > 50) {
            error = 'Name must not exceed 50 characters';
        }

        return error;
    }

    handleEmailInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = this.validateEmail(value);

        this.setState({
            email: value,
            email_error: error,
        });
    }

    validateEmail = (value: string) => {
        let error = '';

        if (value.length === 0 || value.trim().length === 0) {
            error = 'Email must not be left empty or blank';
        }

        if (value.length > 100) {
            error = 'Email must not exceed 100 characters';
        }

        if (value.indexOf('@') === -1) {
            error = 'Invalid email address';
        }

        return error;
    }

    handlePasswordInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = this.validatePassword(value);
        this.setState({
            password: value,
            password_error: error,
        });
    }

    validatePassword = (value: string) => {
        let error = '';

        if (value.length < 6) {
            error = 'Password must have at least 6 characters';
        }

        if (value.indexOf(' ') >= 0) {
            error = 'Password must not contain whitespace';
        }

        return error;
    }

    render() {

        return (
            <EntryForm>
                <FormHeader>Create your account</FormHeader>
                <form onSubmit={this.onSubmit}>
                    <FormGroup>
                        <Label>Name</Label>
                        <input
                            autoComplete="new-password"
                            type="text"
                            className="form-control"
                            onChange={this.handleNameInput}
                        />
                        {this.state.name_error.length > 0 &&
                            <ErrorMessage>{this.state.name_error}</ErrorMessage>
                        }
                    </FormGroup>
                    <FormGroup>
                        <Label>Email</Label>
                        <input
                            autoComplete="new-password"
                            type="email"
                            className="form-control"
                            onChange={this.handleEmailInput}
                        />
                        {this.state.email_error.length > 0 &&
                            <ErrorMessage>{this.state.email_error}</ErrorMessage>
                        }
                    </FormGroup>
                    <FormGroup>
                        <Label>Password</Label>
                        <input
                            autoComplete="new-password"
                            type="password"
                            className="form-control"
                            onChange={this.handlePasswordInput}
                        />
                        {this.state.password_error.length > 0 &&
                            <ErrorMessage>{this.state.password_error}</ErrorMessage>
                        }
                        {this.state.error.length > 0 &&
                            <ErrorMessage>{this.state.error}</ErrorMessage>
                        }
                    </FormGroup>
                    <FormGroup>
                        <button
                            disabled={this.state.disable_button}
                            className="btn-submit stretched"
                        >
                            {this.state.disable_button &&
                                <span className="loading-spinner">
                                    <i className="fas fa-spinner fa-spin" />
                                </span>
                            }
                            Create Account
                        </button>
                    </FormGroup>
                    <div className="center">
                        <Link className="form-link" to="/signin">Already have an account? Sign in</Link>
                    </div>
                </form>
            </EntryForm>
        )
    }
}

export class LoginForm extends Component {

    state = {
        email: '',
        password: '',
        error: '',
        disable_button: false,
    }

    disableButton = (disable: boolean) => {
        this.setState({ disable_button: disable });
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        this.disableButton(true);

        fetch(`${API_URL}/login`, {
            method: 'post',
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(res => {
            if (!res.ok) throw res;
            return res.json();
        })
        .then(response => {
            const localStorage = window.localStorage;
            localStorage.setItem('token', response.token);
            localStorage.setItem('user', JSON.stringify(response.user));
            window.location.replace(`${CLIENT_URL}/account`);
        })
        .catch(error => {

            handleError(error)
            .then(error => {
                this.setState({
                    ...error,
                    disable_button: false,
                });
            });
        });
    }

    handleEmailInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;

        this.setState({
            email: value,
        });
    }

    handlePasswordInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;

        this.setState({
            password: value,
        });
    }

    render() {
        const error = this.state.error;
        return (
            <EntryForm>
                <FormHeader>Login</FormHeader>
                <form onSubmit={this.onSubmit}>
                    <FormGroup>
                        <Label>Email</Label>
                        <input
                            autoComplete="password-new"
                            type="email"
                            className="form-control"
                            onChange={this.handleEmailInput}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Password</Label>
                        <input
                            autoComplete="password-new"
                            type="password"
                            className="form-control"
                            onChange={this.handlePasswordInput}
                        />
                    </FormGroup>
                    {error && error.length > 0 &&
                        <FormGroup>
                            <ErrorMessage>{error}</ErrorMessage>
                        </FormGroup>
                    }
                    <FormGroup>
                        <button
                            disabled={this.state.disable_button}
                            className="btn-submit stretched"
                        >
                            {this.state.disable_button &&
                                <span className="loading-spinner">
                                    <i className="fas fa-spinner fa-spin" />
                                </span>
                            }
                            Login
                        </button>
                    </FormGroup>
                    <div className="center">
                        <Link className="form-link" to="/signup">Create an account</Link>
                    </div>
                </form>
            </EntryForm>
        )
    }
}
