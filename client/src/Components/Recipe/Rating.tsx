import React, { Component } from 'react';
import { userLoggedIn } from '../../utils/Auth';
import { API_URL } from '../Form/Form';

interface IRating {
    recipeId: string;
    rating: any;
    userRating: any;
}

export default class Rating extends Component<IRating> {
    state = {
        recipe_id: '',
        rating: 0,
        user_rating: 0,
    }

    componentDidMount() {
        this.setState({
            recipe_id: this.props.recipeId,
            rating: this.props.rating || 0,
            user_rating: this.props.userRating || 0,
        });
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({
            recipe_id: nextProps.recipeId,
            rating: nextProps.rating || 0,
            user_rating: nextProps.userRating || 0,
        });
    }

    sendRequest = (url: string, method: string, data?: any) => {
        fetch(url, {
            method: method,
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            },
        })
        .then(res => {
            if (!res.ok) throw res;
        })
        .catch(error => {
            try {
                error
                .json()
                .then((err: any) => {
                    console.error(err.message);
                });
            } catch (error) {
                console.error(error);
            }
        });
    }

    setRating = (e: any) => {
        const isLoggedIn = userLoggedIn();

        if (!isLoggedIn) return;

        const element = e.currentTarget;
        const rating = parseInt(element.getAttribute('data-rate'));

        this.setState({
            user_rating: (rating === this.state.user_rating ? 0 : rating)
        }, () => {
            if (this.state.user_rating === 0) {
                this.sendRequest(
                    `${API_URL}/rating/${this.state.recipe_id}`,
                    'DELETE',
                );
            } else {
                this.sendRequest(
                    `${API_URL}/rating`,
                    'POST',
                    {
                        recipe_id: this.state.recipe_id,
                        rating: this.state.user_rating,
                    }
                );
            }
        });
    }

    renderRatingElements = () => {
        let elements = [];
        const upperBound = this.state.user_rating;
        const isLoggedIn = userLoggedIn();

        for (let i = 1; i <= 5; i++) {

            let classNames = '';

            if (isLoggedIn) {
                classNames = upperBound >= i
                ? 'rating-btn fas fa-star'
                : 'rating-btn far fa-star';
            } else {
                classNames = 'rating-btn disabled far fa-star';
            }

            elements.push(
                <i
                    key={i}
                    className={classNames}
                    data-rate={i}
                    onClick={this.setRating}
                />
            );
        }

        return elements;
    }

    render() {
        return (
            <div className="rating">
                {this.renderRatingElements()}
            </div>
        )
    }
}