import React, { PureComponent } from "react";

import { API_URL } from "../../../Form/Form";

import ReactCrop from 'react-image-crop';
import "react-image-crop/dist/ReactCrop.css";

const crop: ICrop = {
    width: 100,
    aspect: 16 / 9,
    x: 0,
    y: 0,
}

export default class FileUpload extends PureComponent<IFileUpload> {

    state = {
        existing_filename: '',
        exisiting_image_url: '',
        existing_original_file: '',
        original_file: '',
        filename: '',
        image_url: '',
        file_type: '',
    }

    componentWillReceiveProps(props: any) {
        const editable = this.props.editable;
        const filename = this.state.existing_filename;

        if (editable && !filename) {
            this.setState({
                existing_filename: props.filename,
            }, () => this.loadOriginalImage());
        }
    }

    loadOriginalImage = () => {
        const url = `${API_URL}/image/original/${this.state.existing_filename}`;
        fetch(url)
            .then(response => {
                if (!response.ok) throw response;
                return response.blob();
            })
            .then(blob => {
                const originalImageURL = window.URL.createObjectURL(blob);
                const file: File = blobToFile(blob, this.state.existing_filename);
                this.setState({
                    exisiting_image_url: originalImageURL,
                    file_type: blob.type,
                    existing_original_file: file,
                });
            })
            .catch(error => console.error(error));
    }

    onFileSelect = (e: any) => {
        e.preventDefault();

        const files = e.currentTarget.files;

        if (files.length === 0) return;

        const file = files[0];

        const fileIsValid = this.validateFile(file);

        if (!fileIsValid) return;

        this.props.setFileError('');

        const url = window.URL.createObjectURL(file);

        this.setState({
            filename: file.name,
            image_url: url,
            file_type: file.type,
            original_file: file,
        });

        this.props.setFile('original_file', file);
    }

    validateFile = (file: File) => {
        const fileType = file.type;
        const fileSize = file.size;

        if (!validFileTypes.includes(fileType)) {
            this.props.setFileError("Invalid file type. Valid extensions include: jpg, jpeg, png.")
            return false;
        }

        const megaByte = 1048576;
        const maxFileSize = megaByte * 4;

        if (fileSize > maxFileSize) {
            this.props.setFileError("File must not exceed 4MB");
            return false;
        } else if (fileSize === 0) {
            this.props.setFileError("File is empty");
            return false;
        }

        return true;
    }

    triggerUpload = (e: any) => {
        e.preventDefault();
        const fileInput: HTMLElement | null = document.querySelector('#file-input');
        if (fileInput) fileInput.click();
    }

    cancel = () => {
        this.setState({
            image_url: '',
            filename: '',
            original_file: '',
        });

        this.props.setFile('file', '');
        this.props.setFile('original_file', '');
        this.props.setFileError('');
    }

    render() {
        return (
            <div id="file-input-container">
                <div id="file-preview">
                    <input id="file-input" type="file" onChange={this.onFileSelect} />
                    {this.state.existing_filename &&
                        <TogglePreview
                            filename={this.state.filename || this.state.existing_filename}
                            file_type={this.state.file_type}
                            image_url={this.state.image_url || this.state.exisiting_image_url}
                            original_file={this.state.original_file || this.state.existing_original_file}
                            triggerUpload={this.triggerUpload}
                            cancel={this.cancel}
                            setFile={this.props.setFile}
                        />
                    }
                    {!this.props.editable &&
                        <>
                            <CropPreview
                                src={this.state.image_url || this.state.exisiting_image_url}
                                original_file={this.state.original_file || this.state.existing_original_file}
                                file_type={this.state.file_type}
                                setFile={this.props.setFile}
                            />
                            <UploadFileButton
                                triggerUpload={this.triggerUpload}
                                filename={this.state.filename || this.state.existing_filename}
                            />
                        </>
                    }
                </div>
            </div>
        )
    }
}

class TogglePreview extends PureComponent<any> {

    state = {
        show_existing: true,
    }

    showExisting = (e: any) => {
        e.preventDefault();
        this.setState({ show_existing: true });
    }

    showCrop = (e: any) => {
        e.preventDefault();
        this.setState({ show_existing: false });
    }

    cancel = (e: any) => {
        this.props.cancel();
        this.showExisting(e);
    }

    render() {
        const renderPreview = this.state.show_existing
        ? <ExistingPreview
            filename={this.props.filename}
            showCrop={this.showCrop}
        />
        : <EditableCropPreview
            image_url={this.props.image_url}
            file_type={this.props.file_type}
            original_file={this.props.original_file}
            showCrop={this.showCrop}
            cancel={this.cancel}
            triggerUpload={this.props.triggerUpload}
            setFile={this.props.setFile}
        />;

        return (
            <>
                {renderPreview}
            </>
        )
    }
}

class CropPreview extends PureComponent<any> {

    state = {
        src: '',
        file_type: '',
        crop: crop,
        image: null,
    }

    componentWillMount() {
        this.props.setFile('original_file', this.props.original_file);
        this.setState({ ...this.props });
    }

    componentWillReceiveProps(props: any) {
        this.props.setFile('original_file', props.original_file);
        this.setState({ ...props });
    }

    onCropChange = (crop: any) => {
        this.setState({ crop });
    };

    onCropComplete = (crop: any) => {
        this.makeClientCrop(crop);
    }

    onImageLoaded = (image: any) => {
        this.setState({ image });
    };

    async makeClientCrop(crop: any) {
        if (this.state.image && crop.width && crop.height) {
            const extension = this.state.file_type.split('/').pop();
            this.getCroppedImg(
                this.state.image,
                crop,
                `cropped_file.${extension}`,
            ).catch(error => console.error(error));
        }
    }

    getCroppedImg = (image: any, crop: any, fileName: any): Promise<boolean | string> => {
        const canvas = document.createElement("canvas");

        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;

        const targetWidth = image.naturalWidth;
        const targetHeight = targetWidth * 0.5625;

        canvas.width = targetWidth;
        canvas.height = targetHeight;

        return new Promise((resolve, reject) => {
            const ctx = canvas.getContext("2d");

            if (!ctx || !image) {
                return reject("Failed");
            }

            ctx.drawImage(
                image,
                crop.x * scaleX,
                crop.y * scaleY,
                crop.width * scaleX,
                crop.height * scaleY,
                0,
                0,
                targetWidth,
                targetHeight
            );

            canvas.toBlob((blob: any) => {
                if (!blob) {
                    return reject('Canvas is empty');
                }

                const file: File = blobToFile(blob, fileName);
                this.props.setFile('file', file);
                resolve(true);
            }, this.state.file_type);
        });
    }

    render() {
        return (
            <div className="image-container">
                <ReactCrop
                    crop={this.state.crop}
                    src={this.state.src}
                    onChange={this.onCropChange}
                    onComplete={this.onCropComplete}
                    onImageLoaded={this.onImageLoaded}
                />
            </div>
        )
    }
}

const EditableCropPreview = (props: any) => {
    return (
        <>
            <CropPreview
                src={props.image_url}
                file_type={props.file_type}
                original_file={props.original_file}
                showCrop={props.showCrop}
                cancel={props.cancel}
                triggerUpload={props.triggerUpload}
                setFile={props.setFile}
            />
            <UploadButtons>
                <UploadFileButton triggerUpload={props.triggerUpload} />
                <CancelButton cancel={props.cancel} />
            </UploadButtons>
        </>
    )
}

const ExistingPreview = (props: any) => (
    <div className="image-container">
        <img
            id="existing-preview"
            onClick={props.showCrop}
            src={`${API_URL}/image/${props.filename}`}
            alt="preview"
        />
    </div>
)

const UploadFileButton = (props: any) => (
    <button className="upload-btn" onClick={props.triggerUpload}>
        <i className="fas fa-file-upload" />
        <span>{props.filename || 'New file' }</span>
    </button>
)

const CancelButton = (props: any) => (
    <button className="upload-btn" onClick={props.cancel}>
        <i className="fas fa-undo" />
        Cancel
    </button>
)

const UploadButtons = (props: any) => (
    <div className="upload-buttons">
        {props.children}
    </div>
)

const blobToFile = (blob: Blob, fileName: string): File => {
    const file = new File([blob], fileName, { lastModified: Date.now() });
    return file;
}

const validFileTypes = ["image/jpeg", "image/png"];

type unit = '%' | 'px' | undefined;

interface IFileUpload {
    editable: boolean;
    filename: string;
    setFile: (key: string, file: File | '') => void;
    setFileError: (file_error: string | '') => void;
}

interface ICrop {
    unit?: unit;
    width: number;
    aspect: number;
    x: number;
    y: number;
}
