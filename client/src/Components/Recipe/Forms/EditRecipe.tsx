import React, { Component } from 'react';
import ErrorContainer from "../../Error/Error";
import Form from './Form';
import './Form.scss';
import { API_URL } from '../../Form/Form';

export default class EditRecipe extends Component<{location: any}> {

    state = {
        id: '',
        name: '',
        cook_time: '',
        servings: '',
        calories: '',
        description: '',
        ingredients: '',
        instructions: '',
        filename: '',
        renderError: false,
    };

    componentDidMount() {
        this.getPostId();
    }

    retrieveData = (id: string) => {
        fetch(`${API_URL}/recipe/${id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            },
        })
            .then(res => {
                if (!res.ok) throw res;
                return res.json();
            })
            .then(response => {
                this.setState({ ...response });
            })
            .catch(error => {
                console.error(error);
            });
    }

    getPostId = () => {
        const stateObject = this.props.location.state || {};

        if (Object.keys(stateObject).length === 0) {

            this.setState({
                renderError: true,
            });

            return console.error("Post ID is missing");
        }

        this.setState({
            id: stateObject.id,
        }, () => {
            const id = this.state.id;
            this.retrieveData(id);
        });
    }

    render() {
        const showForm = !this.state.renderError && this.state.id;
        const renderElement = showForm
        ? (
            <div className="recipe-form-container form-container">
                <Form
                    url={`${API_URL}/recipe/${this.state.id}`}
                    method="put"
                    editable={true}
                    {...this.state}
                />
            </div>
        ) : (
            <ErrorContainer>
                It looks like you tried to edit a non-existant recipe. Go back to the
                recipe you want to edit and click the edit button.
            </ErrorContainer>
        );

        return renderElement;
    }
}