import React from 'react';
import Form from './Form';
import { API_URL } from '../../Form/Form';

const CreateRecipe: React.FC = () => (
    <div className="recipe-form-container form-container">
        <Form url={`${API_URL}/recipe`} method="post" />
    </div>
)

export default CreateRecipe;