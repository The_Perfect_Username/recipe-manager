import React, { Component } from 'react';
import ErrorContainer from "../../Error/Error";
import Form from './Form';
import { API_URL } from '../../Form/Form';

export default class CreateRevision extends Component<{location: any}> {
    state = {
        id: '',
        name: '',
        description: '',
        ingredients: '',
        instructions: '',
        filename: '',
        renderError: false,
    };

    componentDidMount() {
        this.getPostId();
    }

    retrieveData = (id: string) => {
        fetch(`${API_URL}/recipe/${id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            },
        })
        .then(res => {
            if (!res.ok) throw res;
            return res.json();
        })
        .then(response => {
            this.setState({ ...response });
        })
        .catch(error => {
            console.error(error);
        });
    }

    getPostId = () => {
        const stateObject = this.props.location.state || {};

        if (Object.keys(stateObject).length === 0) {

            this.setState({
                renderError: true,
            });

            return console.error("Post ID is missing");
        }

        this.setState({
            id: stateObject.id,
        }, () => {
            const id = this.state.id;
            this.retrieveData(id);
        });
    }

    render() {
        const showForm = !this.state.renderError && this.state.id;
        const renderForm = showForm
        ? (
            <div className="recipe-form-container form-container">
                <Form
                    url={`${API_URL}/recipe/revision`}
                    method="post"
                    editable={true}
                    isRevision={true}
                    {...this.state}
                />
            </div>
        ) : (
            <ErrorContainer>
                Looks like you tried to create a revision from a non-existant post. You'll want to
                go back and click the 'Revision' button from the recipe you want.
            </ErrorContainer>
        );

        return renderForm;
    }
}
