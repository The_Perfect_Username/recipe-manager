export const validateName = (value: string) => {
    let error = '';

    if (value.length === 0 || value.trim().length === 0) {
        error = 'Recipe name must not be left empty or blank';
    }

    if (value.length > 100) {
        error = 'Recipe name must not exceed 100 characters';
    }

    return error;
};

export const validateIngredients = (value: string) => {
    let error = '';

    if (value.length === 0 || value.trim().length === 0) {
        error = 'Ingredients must not be left empty or blank';
    }

    if (value.length > 2000) {
        error = 'Ingredients must not exceed 2000 characters';
    }

    return error;
};

export const validateInstructions = (value: string) => {
    let error = '';

    if (value.length === 0 || value.trim().length === 0) {
        error = 'Instructions must not be left empty or blank';
    }

    if (value.length > 2000) {
        error = 'Instructions must not exceed 5000 characters';
    }

    return error;
};

export const validateDescription = (value: string) => {
    let error = '';

    if (value.length > 240) {
        error = 'Description must not exceed 240 characters';
    }

    return error;
};
