import React, { Component } from 'react';
import {
    Label,
    FormGroup,
    ErrorMessage,
    PreviewText,
    CLIENT_URL,
    LabelWithCounter,
} from '../../Form/Form';
import FileUpload from './FileUpload/FileUpload';
import { handleError } from '../../../utils/ErrorHandling';
import {
    validateName,
    validateInstructions,
    validateDescription,
    validateIngredients
} from './ValidateInput';
import InputMask from 'react-input-mask';
import './Form.scss';

const processTime = (value: string) => {
    const hoursAndMinutes = value.split(':');
    const hours = parseInt(hoursAndMinutes[0]);
    const minutes = parseInt(hoursAndMinutes[1]);

    let cook_time = '00:00';

    if (minutes > 59) {
        const subMinutes = minutes - 60;
        const newHour = hours + 1;
        const nminutes = subMinutes < 10 ? `0${subMinutes}` : subMinutes;

        if (newHour < 10) {
            cook_time = `0${newHour}:${nminutes}`;
        } else {
            cook_time = `${newHour}:${nminutes}`;
        }

    } else {
        const nminutes = minutes < 10 ? `0${minutes}` : minutes;

        if (hours < 10) {
            cook_time = `0${hours}:${nminutes}`;
        } else {
            cook_time = `${hours}:${nminutes}`;
        }
    }

    return cook_time === '00:00' ? '' : cook_time;
}

export default class Form extends Component<IForm> {

    state = {
        id: '',
        url: '',
        method: '',
        editable: false,
        isRevision: false,
        file: '',
        original_file: '',
        name: '',
        prep_time: '',
        cook_time: '',
        servings: '',
        calories: '',
        description: '',
        ingredients: '',
        instructions: '',
        privacy: 'public',
        filename: '',
        file_error: '',
        name_error: '',
        description_error: '',
        ingredients_error: '',
        instructions_error: '',
        error: '',
        name_count: 100,
        description_count: 240,
        ingredients_count: 2000,
        instructions_count: 2000,
        disable_button: false,
    }

    componentWillMount() {
        this.setState({ ...this.props });
    }

    componentWillReceiveProps(nextProps: any) {
        const {
            name,
            description,
            ingredients,
            instructions,
        } = nextProps;
        this.setState({
            ...nextProps,
            name_count: 100 - (name || '').length,
            description_count: 240 - (description || '').length,
            ingredients_count: 2000 - (ingredients || '').length,
            instructions_count: 2000 - (instructions || '').length,
        });
    }

    setFileError = (file_error: string | '') => {
        this.setState({
            file_error,
        });
    }

    requiredFieldsCheck = (callback: any) => {

        const nameLength = this.state.name.length;
        const ingredientsLength = this.state.ingredients.length;
        const instructionsLength = this.state.instructions.length;

        const name_error = nameLength === 0 ? 'Recipe name must not be left empty or blank' : '';
        const ingredients_error = ingredientsLength === 0 ? 'Ingredients must not be left empty or blank' : '';
        const instructions_error = instructionsLength === 0 ? 'Instructions must not be left empty or blank' : '';
        const file_error = this.state.method !== 'put' && (!this.state.file || this.state.file) === '' ? 'File is missing' : '';

        this.setState({
            name_error,
            ingredients_error,
            instructions_error,
            file_error,
        }, () => {
            callback(this.formHasErrors());
        });
    }

    formHasErrors = () => {
        const nameErrorLength = this.state.name_error.length;
        const descriptionErrorLength = this.state.description_error.length;
        const ingredientsErrorLength = this.state.ingredients_error.length;
        const instructionsErrorLength = this.state.instructions_error.length;
        const fileErrorLength = this.state.file_error.length;

        return (
            nameErrorLength
            + descriptionErrorLength
            + ingredientsErrorLength
            + instructionsErrorLength
            + fileErrorLength
        ) > 0;
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        this.requiredFieldsCheck((hasErrors: Boolean) => {

            if (hasErrors) return;

            this.disableButton(true);

            const data = new FormData();

            if (this.state.isRevision) {
                data.append('id', this.state.id);
            }

            data.append('name', this.state.name);
            data.append('prep_time', this.state.prep_time);
            data.append('cook_time', this.state.cook_time);
            data.append('servings', this.state.servings);
            data.append('calories', this.state.calories);
            data.append('description', this.state.description);
            data.append('ingredients', this.state.ingredients);
            data.append('instructions', this.state.instructions);
            data.append('privacy', this.state.privacy);
            data.append('file', this.state.file);
            data.append('original_file', this.state.original_file);

            fetch(this.state.url, {
                method: this.state.method,
                body: data,
                headers: {
                    'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
                },
            })
            .then(res => {
                if (!res.ok) throw res;
                return res.json();
            })
            .then(response => {
                const id = response.id;
                window.location.replace(`${CLIENT_URL}/post/${id}`);
            })
            .catch(error => {
                handleError(error)
                .then(error => {
                    const bannedMessage = error.is_banned
                    ? error.message
                    : '';

                    this.setState({
                        ...error,
                        error: bannedMessage,
                        disable_button: false,
                    });
                });
            });
        });
    }

    disableButton = (disable: boolean) => {
        this.setState({
            disable_button: disable,
        });
    }

    handleTimeInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const name = input.name;
        const object: any = {};
        const time = processTime(value);

        object[name] = time;

        this.setState(object);
    }

    handleNumberInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const name = input.name;
        const object: any = {};

        object[name] = value;

        this.setState(object);
    }

    handleNameInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = validateName(value);
        const charLeft = 100 - value.length;

        this.setState({
            name: value,
            name_error: error,
            name_count: charLeft,
        });
    }

    handleDescriptionInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = validateDescription(value);
        const charLeft = 240 - value.length;

        this.setState({
            description: value,
            description_error: error,
            description_count: charLeft,
        });
    }

    handleIngredientsInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = validateIngredients(value);
        const charLeft = 2000 - value.length;

        this.setState({
            ingredients: value,
            ingredients_error: error,
            ingredients_count: charLeft,
        });
    }

    handleInstructionsInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        const error = validateInstructions(value);
        const charLeft = 2000 - value.length;

        this.setState({
            instructions: value,
            instructions_error: error,
            instructions_count: charLeft,
        });
    }

    handlePrivacyInput = (e: any) => {
        const input = e.currentTarget;
        const value = input.value;
        this.setState({
            privacy: value,
        });
    }

    setFile = (key: string, file: File | '') => {
        const fileObject: any = {};
        fileObject[key] = file;
        this.setState({ ...fileObject });
    }

    cancel = (e: any) => {
        e.preventDefault();
        if (this.state.id && this.state.id.length > 0) {
            window.location.replace(`${CLIENT_URL}/post/${this.state.id}`);
        } else {
            window.location.replace(`${CLIENT_URL}/account`);
        }
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <FormGroup>
                    <Label>Display Image</Label>
                    <FileUpload
                        editable={this.state.editable}
                        filename={this.state.filename}
                        setFile={this.setFile}
                        setFileError={this.setFileError}
                    />
                    {this.state.file_error.length > 0 &&
                        <ErrorMessage>{this.state.file_error}</ErrorMessage>
                    }
                </FormGroup>
                <FormGroup>
                    <LabelWithCounter
                        label="Recipe Name"
                        counter={this.state.name_count}
                    />
                    <input
                        type="text"
                        className="form-control"
                        onChange={this.handleNameInput}
                        value={this.state.name}
                        maxLength={100}
                    />
                    {this.state.name_error.length > 0 &&
                        <ErrorMessage>{this.state.name_error}</ErrorMessage>
                    }
                </FormGroup>
                <RecipeTimeServingsCaloriesInputs
                    handleTimeInput={this.handleTimeInput}
                    handleNumberInput={this.handleNumberInput}
                    prep_time={this.state.prep_time}
                    cook_time={this.state.cook_time}
                    servings={this.state.servings}
                    calories={this.state.calories}
                />
                <FormGroup>
                    <LabelWithCounter
                        label="Description"
                        counter={this.state.description_count}
                    />
                    <textarea
                        className="form-control"
                        rows={3}
                        onChange={this.handleDescriptionInput}
                        value={this.state.description}
                        maxLength={240}
                    ></textarea>
                    {this.state.description_error.length > 0 &&
                        <ErrorMessage>{this.state.description_error}</ErrorMessage>
                    }
                    {this.state.description.length > 0 &&
                        <PreviewText value={this.state.description} type="p" />
                    }
                </FormGroup>
                <FormGroup>
                    <LabelWithCounter
                        label="Ingredients"
                        counter={this.state.ingredients_count}
                    />
                    <textarea
                        className="form-control"
                        rows={5}
                        onChange={this.handleIngredientsInput}
                        value={this.state.ingredients}
                        maxLength={2000}
                    ></textarea>
                    {this.state.ingredients_error.length > 0 &&
                        <ErrorMessage>{this.state.ingredients_error}</ErrorMessage>
                    }
                    {this.state.ingredients.length > 0 &&
                        <PreviewText value={this.state.ingredients} type="ul" />
                    }
                </FormGroup>
                <FormGroup>
                    <LabelWithCounter
                        label="Instructions"
                        counter={this.state.instructions_count}
                    />
                    <textarea
                        className="form-control"
                        rows={5}
                        onChange={this.handleInstructionsInput}
                        value={this.state.instructions}
                        maxLength={2000}
                    ></textarea>
                    {this.state.instructions_error.length > 0 &&
                        <ErrorMessage>{this.state.instructions_error}</ErrorMessage>
                    }
                    {this.state.instructions.length > 0 &&
                        <PreviewText value={this.state.instructions} type="ol" />
                    }
                </FormGroup>
                <FormGroup>
                    <Label>Privacy</Label>
                    <select
                        className="form-control"
                        value={this.state.privacy}
                        onChange={this.handlePrivacyInput}
                    >
                        <option value="public">Public</option>
                        <option value="private">Private</option>
                    </select>
                </FormGroup>
                {this.state.error.length > 0 &&
                    <FormGroup>
                        <ErrorMessage>{this.state.error}</ErrorMessage>
                    </FormGroup>
                }
                <FormGroup className="reversed">
                    <button disabled={this.state.disable_button} className="btn-submit">
                        {this.state.disable_button &&
                            <span className="loading-spinner">
                                <i className="fas fa-spinner fa-spin" />
                            </span>
                        }
                        Submit
                    </button>
                    <button onClick={this.cancel} className="btn-link">Cancel</button>
                </FormGroup>
            </form>
        )
    }
}

const RecipeTimeServingsCaloriesInputs = (props: any) => {
    return (
        <div className="inline-flex">
            <FormGroup>
                <Label>Prep Time (HH:MM)</Label>
                <InputMask mask="99:99" maskChar={'0'} value={props.prep_time} name="prep_time" onChange={props.handleTimeInput}>
                    {(inputProps: any) => <input className="form-control" {...inputProps} type="text" />}
                </InputMask>
            </FormGroup>
            <FormGroup>
                <Label>Cook Time (HH:MM)</Label>
                <InputMask mask="99:99" maskChar={'0'} value={props.cook_time} name="cook_time" onChange={props.handleTimeInput}>
                    {(inputProps: any) => <input className="form-control" {...inputProps} type="text" />}
                </InputMask>
            </FormGroup>
            <FormGroup>
                <Label>Servings</Label>
                <InputMask mask="999" maskChar={null} value={props.servings} name="servings" onChange={props.handleNumberInput}>
                    {(inputProps: any) => <input className="form-control" {...inputProps} type="text" />}
                </InputMask>
            </FormGroup>
            <FormGroup>
                <Label>Calories</Label>
                <InputMask mask="99999" maskChar={null} value={props.calories} name="calories" onChange={props.handleNumberInput}>
                    {(inputProps: any) => <input className="form-control" {...inputProps} type="text" />}
                </InputMask>
            </FormGroup>
        </div>
    )
}

interface IForm {
    url: string;
    method: string;
    isRevision?: boolean;
    branchComponent?: any;
    editable?: boolean;
    name?: string | '';
    description?: string;
    ingredients?: string;
    instructions?: string;
    filename?: string;
}
