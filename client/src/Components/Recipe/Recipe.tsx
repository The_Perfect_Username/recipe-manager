import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Tile } from '../Cards/Card';
import { processText, API_URL } from '../Form/Form';
import { userLoggedIn, logout } from '../../utils/Auth';
import Comments from '../Comments/Comment';
import Rating from './Rating';
import './Recipe.scss';
import DeletePostModal from '../Modals/DeletePostModal';
import { handleError } from '../../utils/ErrorHandling';

const moment = require('moment');

export default class Post extends Component<{ match: any }> {

    state = {
        id: '',
        user_id: '',
        name: '',
        user_name: '',
        display_name: '',
        description: '',
        ingredients: '',
        instructions: '',
        prep_time: null,
        cook_time: null,
        servings: null,
        calories: null,
        rating: null,
        user_rating: null,
        revision_parent: '',
        saved: false,
        filename: '',
        created: '',
        belongs_to_user: false,
        loaded: false,
        show_modal: false,
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    componentWillMount() {
        const id = this.props.match.params.id;
        this.sendRequest(id);
    }

    componentWillReceiveProps(nextProps: any) {
        const id = nextProps.match.params.id;
        this.sendRequest(id);
    }

    handleOpenModal = (e: any) => {
        e.preventDefault();
        this.setState({ show_modal: true });
    }

    handleCloseModal = (e: any) => {
        e.preventDefault();
        this.setState({ show_modal: false });
    }

    sendRequest = (id: string) => {

        const token = window.localStorage.getItem('token');
        const headers: any = {
            'Content-Type': 'application/json',
        };

        if (token) {
            headers.Authorization = `Bearer ${token}`;
        }

        fetch(`${API_URL}/recipe/${id}`, {
            headers: headers,
        })
        .then(res => {
            if (!res.ok) throw res;
            return res.json();
        })
        .then(response => {
            this.setState({ ...response, loaded: true });
        })
        .catch(error => {
            handleError(error)
            .then(error => {
                if (error.is_banned) {
                    logout();
                }
            })
        });
    }

    render() {

        const displayName = this.state.display_name
            ? this.state.display_name
            : this.state.user_name;

        return (
            <>
            <div className="recipe-container">
                <div className="recipe-content">
                    <MainImage
                        filename={this.state.filename}
                        alt={this.state.name}
                        prep_time={this.state.prep_time}
                        cook_time={this.state.cook_time}
                        servings={this.state.servings}
                        calories={this.state.calories}
                    />
                    <div className="thick-padding">
                        <PostTitle>{this.state.name}</PostTitle>
                        <div className="content-group">
                            <PostMeta
                                recipeId={this.state.id}
                                userId={this.state.user_id}
                                name={this.state.name}
                                rating={this.state.rating}
                                userRating={this.state.user_rating}
                                userName={displayName}
                                created={this.state.created}
                                belongsToUser={this.state.belongs_to_user}
                                revisionParent={this.state.revision_parent}
                            />
                            {this.state.loaded &&
                                <RecipeMenu
                                    recipeId={this.state.id}
                                    name={this.state.name}
                                    belongsToUser={this.state.belongs_to_user}
                                    revisionParent={this.state.revision_parent}
                                    saved={this.state.saved}
                                    handleOpenModal={this.handleOpenModal}
                                />
                            }
                        </div>
                        <Description value={this.state.description} />
                        <Ingredients value={this.state.ingredients} />
                        <Instructions value={this.state.instructions} />
                    </div>
                </div>
            </div>
            <Comments recipe_id={this.state.id} />
            {this.state.belongs_to_user &&
                <DeletePostModal
                    showModal={this.state.show_modal}
                    handleCloseModal={this.handleCloseModal}
                    recipeId={this.state.id}
                />
            }
            </>
        );
    }
}

class MainImage extends Component<IMainImage, any> {

    state = {
        loading: true,
    }

    onLoad = () => {
        this.setState({
            loading: false,
        })
    }

    render() {
        const { filename, alt } = this.props;

        return (
            <div className="main-image-container">
                {this.state.loading && <Tile />}
                {filename && (
                    <>
                        <img
                            onLoad={this.onLoad}
                            src={`${API_URL}/image/${filename}`}
                            alt={alt}
                        />
                        <RecipeTimeServingsCalories
                            prep_time={this.props.prep_time}
                            cook_time={this.props.cook_time}
                            servings={this.props.servings}
                            calories={this.props.calories}
                        />
                    </>
                )}
            </div>
        )
    }

}

interface IMainImage {
    filename: string;
    alt: string;
    servings: number | null;
    prep_time: number | null;
    cook_time: number | null;
    calories: number | null;
}

const PostTitle: React.FC = (props) => {
    return (
        <div className="post-title">
            <h2>{props.children || <Tile />}</h2>
        </div>
    )
}

const PostMeta: React.FC<{
    recipeId: string;
    name: string;
    userId: string;
    rating: any;
    userRating: any;
    userName: string;
    created: string;
    belongsToUser: boolean;
    revisionParent: string | null;
}> = (props) => {

    const {
        userId,
        recipeId,
        rating,
        userRating,
        userName,
        created,
    } = props;

    const mDate = moment(created);
    const timeAgo = moment(mDate).fromNow();
    const averageRating = rating ? rating.toFixed(1) : null;

    const renderDetails = (userName && timeAgo)
        ? (
            <>
                <Link to={`/user/${userId}`} className="display-name">{userName}</Link>
                <span className="time-ago">{timeAgo}</span>
            </>
        )
        : <Tile />;

    return (
        <div className="post-meta">
            <div className="post-details">
                {renderDetails}
            </div>
            <RecipeControls
                recipeId={recipeId}
                rating={rating}
                userRating={userRating}
                averageRating={averageRating}
            />
        </div>
    )
}

const LoadingTileLines = () => {
    let elements = [];

    for (let i = 0; i < 4; i++) {
        elements.push(
            <span key={i}><Tile/></span>
        );
    }

    return (
        <div className="loading-tile-lines">
            {elements}
        </div>
    );
}

const Description: React.FC<IRecipe> = (props) => {
    const { value } = props;
    const description = processText(value, 'p');
    const renderElement = description
        ? description
        : <LoadingTileLines />;

    return (
        <div className="description content-group">
            {renderElement}
        </div>
    )
}

const Ingredients: React.FC<IRecipe> = (props) => {
    const { value } = props;
    const ingredients = processText(value, 'ul');
    const renderElement = value
    ? (
        <>
            <h3 className="content-header">Ingredients</h3>
            {ingredients}
        </>
    ) : (
        <>
            <h3 className="content-header loading"><Tile /></h3>
            <LoadingTileLines />
        </>
    )

    return (
        <div className="content-group">
            {renderElement}
        </div>
    )
}

const Instructions: React.FC<IRecipe> = (props) => {
    const { value } = props;
    const instructions = processText(value, 'ol');
    const renderElement = value
    ? (
        <>
            <h3 className="content-header">Instructions</h3>
            {instructions}
        </>
    ) : (
        <>
            <h3 className="content-header loading"><Tile /></h3>
            <LoadingTileLines />
        </>
    )

    return (
        <div className="content-group">
            {renderElement}
        </div>
    )
}

const RecipeControls: React.FC<{
    recipeId: string;
    rating: any;
    userRating: any;
    averageRating: any;
}> = (props) => {
    const {
        recipeId,
        rating,
        userRating,
        averageRating
    } = props;

    const renderElements = recipeId
    ? (<>
            <div className="average-rating">
                {averageRating}
            </div>
            <Rating
                recipeId={recipeId}
                rating={rating}
                userRating={userRating}
            />
        </>
    ) : <Tile />
    return (
        <div className="recipe-controls">
            {renderElements}
        </div>
    )
}

class RecipeMenu extends Component<any> {
    render() {

        const {
            recipeId,
            name,
            belongsToUser,
            revisionParent,
            saved,
            handleOpenModal,
        } = this.props;

        const isLoggedIn = userLoggedIn();

        const belongsToLoggedInUser = belongsToUser && isLoggedIn;

        return (
            <div className="menu-container d-spaced">
                <nav className="recipe-menu d-v-center">
                    {isLoggedIn &&
                        <>
                            <Link
                                to={{
                                    pathname: '/post/revision',
                                    state: {
                                        id: recipeId,
                                        name: name,
                                    },
                                }}
                                className="action-ctl-btn"
                            >
                                Fork
                            </Link>
                            {revisionParent &&
                                <Link
                                    to={{
                                        pathname: `/post/${revisionParent}`,
                                        state: {
                                            id: revisionParent,
                                        },
                                    }}
                                    className="action-ctl-btn"
                                >
                                    Original
                                </Link>
                            }
                        </>
                    }
                    {belongsToLoggedInUser && (
                        <>
                            <Link
                                to={{
                                    pathname: '/post/edit',
                                    state: {
                                        id: recipeId,
                                    },
                                }}
                            >
                                Edit
                            </Link>
                            <a onClick={handleOpenModal}>
                                Delete
                            </a>
                        </>
                    )}
                    {isLoggedIn && <SaveButton recipeId={recipeId} saved={saved} />}
                </nav>
            </div>
        );
    }
}

class SaveButton extends Component<any> {

    state = {
        saved: false,
        recipe_id: '',
    }

    componentDidMount() {
        this.setState({
            recipe_id: this.props.recipeId,
            saved: this.props.saved,
        });
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({
            recipe_id: nextProps.recipeId,
            saved: nextProps.saved,
        });
    }

    onClick = (e: any) => {
        e.preventDefault();
        if (this.state.saved) {
            const method = 'DELETE';
            const url = `${API_URL}/save/${this.state.recipe_id}`;
            this.sendRequest(url, method);
        } else {
            const method = 'POST';
            const url = `${API_URL}/save`;
            this.sendRequest(url, method, { recipe_id: this.state.recipe_id });
        }
    }

    sendRequest = (url: string, method: string, data?: any) => {
        fetch(url, {
            method: method,
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            },
        })
        .then(res => {
            if (!res.ok) throw res;
            this.setState({ saved: !this.state.saved });
        })
        .catch(error => {
            try {
                error
                .json()
                .then((err: any) => {
                    console.error(err.message);
                });
            } catch (error) {
                console.error(error);
            }
        });
    }

    render() {
        const saveLabel = this.state.saved
        ? 'Unsave'
        : 'Save';

        return (
            <a onClick={this.onClick}>{saveLabel}</a>
        )
    }
}

const RecipeTimeServingsCalories = (props: any) => {
    const {
        prep_time,
        cook_time,
        servings,
        calories,
    } = props;

    const hasData = !!prep_time || !!cook_time || !!servings || !!calories;

    return (
        <div className={`recipe-details ${!hasData && 'hidden'}`}>
            {prep_time &&
                <Thing label="Prep Time" value={prep_time} />
            }
            {cook_time &&
                <Thing label="Cook Time" value={cook_time} />
            }
            {servings &&
                <Thing label="Serves" value={servings} />
            }
            {calories &&
                <Thing label="Calories" value={calories} />
            }
        </div>
    );
}

const Thing = (props: any) => {
    const {
        label,
        value,
    } = props;

    return (
        <div className="recipe-detail">
            <span className="label">{label}</span>
            <span className="value">{value}</span>
        </div>
    )
}

interface IRecipe {
    value: string;
}