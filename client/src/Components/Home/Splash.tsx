import React, { Component } from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { userLoggedIn, logout } from '../../utils/Auth';

import Background from '../../Images/main-background.jpg';
import SmallBackground from '../../Images/main-small.jpg';
import WhiteLogo from '../../Images/logo-white.svg';
import './Splash.scss';

const FrontPage = (props: any) => {

    const isLoggedIn = userLoggedIn();

    const actionUrl = isLoggedIn
        ? '/account'
        : '/signup';

    return (
        <div id="splash-page">
            <Header loggedIn={isLoggedIn} />
            <main>
                <div className="header-section">
                    <>
                        <div className="overlay"></div>
                        <Image />
                    </>
                    <div className="header-content">
                        <h1>Recipe Creativity Just Got Easier</h1>
                        <SearchForm {...props}  />
                    </div>
                    <div className="ab-container">
                        <Link to={actionUrl} className="action-btn">
                            {isLoggedIn && "To My Account"}
                            {!isLoggedIn && "Start Your Journey"}
                        </Link>
                    </div>
                </div>
                <div className="container">
                    <div className="std-block main">
                        <h2>Have you ever tried a recipe you found online and thought "Wow, that could be a lot better"?</h2>
                    </div>
                    <div className="std-block">
                        <h2>An online repository of recipes from around the world</h2>
                        <p>
                            Forget about scouring the web and using scrapers to save recipes to your e-cookbook. This community
                            driven platform will provide you with the recipes you desire.
                        </p>
                    </div>
                    <div className="std-block">
                        <h2>Innovation and Creativity</h2>
                        <p>
                            Found a recipe that you like and know it could be even better? Know how to turn that $10 meal into
                            a 5-star cuisine? Create a revision of that recipe and share it with the world!
                        </p>
                    </div>
                    <div className="std-block">
                        <h2>Open-sourced and Community Driven</h2>
                        <p>
                            We believe in a lot of give and take. Our community shares their recipes to be enjoyed by the
                            world and open for changes. But if you're not quite ready to share your grandmother's secret recipe,
                            you can keep that for yourself. We understand, we won't judge!
                        </p>
                    </div>
                </div>
            </main>
            <Footer />
        </div>
    )
}

class Image extends Component {
    state = {
        loading: true,
    }

    onLoad = () => {
        this.setState({
            loading: false,
        });
    }

    render() {

        return (
            <>
                <img onLoad={this.onLoad} src={Background} alt="Main background" />
                {this.state.loading &&
                    <img src={SmallBackground} alt="Lazy load" />
                }
            </>
        )
    }
}

class SearchForm extends Component<RouteComponentProps<any>> {

    state = {
        search: '',
    }

    sendRequest = (e: any) => {
        e.preventDefault();
		if (this.state.search.length === 0) return;
		const search = `?q=${this.state.search}`;
		this.props.history.push(`/search${search}`);
		this.props.location.pathname = `/search${search}`;
		this.props.location.search = search;
    }

    onChange = (e: any) => {
        const input = e.target;
        const value = input.value;

        this.setState({ search: value });
    }

    render() {
        return (
            <form onSubmit={this.sendRequest}>
                <input onChange={this.onChange} placeholder="Discover your new cuisine..." />
            </form>
        )
    }
}

const Header = (props: any) => {
    const { loggedIn } = props;
    return (
        <header>
            <div className="site-logo">
                <img
                    className="logo"
                    src={WhiteLogo}
                    alt="logo"
                    height={40}
                />
            </div>
            <div className="entry-btns">
                {!loggedIn &&
                    <>
                        <Link to="/signup">Create Account</Link>
                        <Link to="/signin">Log In</Link>
                    </>
                }
                {loggedIn &&
                    <>
                        <Link to="/account">Account</Link>
                        <a href="#" onClick={logout}>Log Out</a>
                    </>
                }
            </div>
        </header>
    );
}


const Footer: React.FC = () => (
    <footer>
        <div className="container">
            <div>
                <p>&copy; 2019 Open Saucepan</p>
            </div>
            <div>
                <Link to="/privacy-policy">Privacy Policy</Link>
            </div>
        </div>
    </footer>
);

export default withRouter(FrontPage);