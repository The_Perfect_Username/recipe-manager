import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CommentsForm from './CommentsForm';
import EditCommentForm from './EditCommentForm';
import './Comment.scss';
import { API_URL, processText } from '../Form/Form';
import { userLoggedIn } from '../../utils/Auth';

const moment = require('moment');

export default class Comments extends Component<any, any> {
    private abortController: any;
    constructor(props: any) {
        super(props);
        window.onscroll = () => this.onScroll();
        this.abortController = new AbortController();
    }

    state = {
        error: false,
        recipe_id: '',
        results: [],
        loading: false,
        has_more: true,
        offset: 0,
    };

    componentWillReceiveProps(props: any) {
        this.setState({ recipe_id: props.recipe_id });
    }

    componentWillUnmount() {
        this.abortController.abort();
    }

    fetchComments = () => {
        const id = this.state.recipe_id;
        const limit = 25;
        const offset = this.state.offset;
        this.setState({ loading: true });

        const token = window.localStorage.getItem('token');
        const headers: any = {};

        if (token) {
            headers.Authorization = `Bearer ${token}`;
        }

        fetch(`${API_URL}/comments?recipe_id=${id}&limit=${limit}&offset=${offset}`, {
            method: 'GET',
            signal: this.abortController.signal,
            headers,
        }).then(res => {
            if (!res.ok) throw res;
            return res.json();
        }).then(response => {
            const newOffset = limit + offset;

            this.setState((prevState: any) => ({
                results: [...prevState.results, ...response],
                offset: newOffset,
                loading: false,
                error: false,
                has_more: response.length === limit,
            }));
        })
        .catch(_ => this.setState({ error: true }));
    }

    setResults = (comment: any) => {
        this.setState((prevState: any) => ({ results: [comment, ...prevState.results] }));
    }

    removeCommentFromResults = (index: number) => {
        const results = this.state.results.filter((_, i) => index !== i);
        this.setState({ results });
    }

    onScroll = () => {
        const {
            error,
            loading,
            has_more,
        } = this.state;

        if (error || loading || !has_more) return;

        const docElement = document.documentElement;
        const scrollHeight = docElement.scrollHeight;
        const scrollTop = docElement.scrollTop;
        const clientTop = docElement.clientHeight;

        const difference = scrollHeight - scrollTop;

        if (this.state.recipe_id === '') return;
        if (difference === clientTop) {
            this.fetchComments();
        }
    }

    render() {
        const renderComments = this.state.results.map((comment: any, index: number) => {
            return (
                <Comment
                    key={index}
                    index={index}
                    {...comment}
                    removeCommentFromResults={this.removeCommentFromResults}
                />
            );
        });

        const renderElements = renderComments.length > 0
        ? renderComments
        : <NoResults />;

        const userIsLoggedIn = userLoggedIn();

        return (
            <div className="recipe-container comments">
                <div className="comments-form">
                    {userIsLoggedIn &&
                        <CommentsForm
                            recipe_id={this.state.recipe_id}
                            setResults={this.setResults}
                        />
                    }
                    {!userIsLoggedIn &&
                        <div className="blank-block">
                            <span><Link to='/signin'>Login</Link> to post a comment</span>
                        </div>
                    }
                </div>
                {renderElements}
                {this.state.loading &&
                    <Loading />
                }
            </div>
        )
    }
}

class Comment extends Component<any> {

    state = {
        id: '',
        edited: false,
        comment: '',
        belongs_to_user: false,
        index: null,
        delete_actioned: false,
        edit_actioned: false,
    }

    componentDidMount() {
        this.setState({
            id: this.props.id,
            index: this.props.index,
            comment: this.props.comment,
            belongs_to_user: this.props.belongs_to_user,
            edited: this.props.edited,
        });
    }

    componentWillReceiveProps(props: any) {
        this.setState({
            id: props.id,
            index: props.index,
            comment: props.comment,
            edited: props.edited,
            belongs_to_user: props.belongs_to_user,
        });
    }

    actionDelete = (e: any) => {
        e.preventDefault();
        this.setState({ delete_actioned: !this.state.delete_actioned });
    }

    actionEdit = (e: any) => {
        e.preventDefault();
        this.setState({ edit_actioned: !this.state.edit_actioned });
    }

    updateComment = (comment: string) => {
        this.setState({ comment, edited: true });
    }

    onDelete = (e: any) => {
        e.preventDefault();

        fetch(`${API_URL}/comments/${this.state.id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            }
        })
        .then(_ => {
            this.setState({ delete_actioned: false, edit_actioned: false, });
            this.props.removeCommentFromResults(this.state.index);
        })
        .catch(error => console.error(error));
    }

    render() {
        const {
            id,
            author,
            display_name,
            created,
        } = this.props;

        const mDate = moment(created);
        const timeAgo = moment(mDate).fromNow();
        const renderDeleteButton = this.state.delete_actioned
            ? <ConfirmDeletion actionDelete={this.actionDelete} onDelete={this.onDelete} />
            : <a onClick={this.actionDelete}>Delete</a>;

        return (
            <div className="comment">
                {this.state.edit_actioned &&
                    <EditCommentForm
                        id={id}
                        actionEdit={this.actionEdit}
                        updateComment={this.updateComment}
                        comment={this.state.comment}
                    />
                }
                {!this.state.edit_actioned &&
                    <>
                        <div className="meta">
                            <span className="author">{display_name || author}</span>
                            <span className="created">{timeAgo}</span>
                            {!!this.state.edited && <span className="edited">Edited</span>}
                        </div>
                        <div className="comment-text">
                            {processText(this.state.comment, 'p')}
                        </div>
                        {this.state.belongs_to_user &&
                            <nav>
                                <a onClick={this.actionEdit}>Edit</a>
                                {renderDeleteButton}
                            </nav>
                        }
                    </>
                }
            </div>
        )
    }
}

const ConfirmDeletion = (props: any) => {
    return (
        <div className="menu-confirmation">
            <span>Delete Comment?</span>
            <a onClick={props.onDelete}>Confirm</a>
            <a onClick={props.actionDelete}>Cancel</a>
        </div>
    )
}

const Loading = () => (
    <div className="blank-block">
        <span>
            <i className="fas fa-spinner fa-spin" />
        </span>
    </div>
)

const NoResults = () => (
    <div className="blank-block">
        <span>No comments found</span>
    </div>
);