import React, { Component } from 'react';
import { handleError } from '../../utils/ErrorHandling';
import { FormGroup, Label, ErrorMessage, API_URL } from '../Form/Form';

export default class CommentForm extends Component<any> {
    state = {
        id: '',
        comment: '',
        char_count: 500,
        comment_error: '',
    }

    componentDidMount() {
        this.setState({
            id: this.props.id,
            comment: this.props.comment,
            char_count: 500 - this.props.comment.length,
        });
    }

    onChange = (e: any) => {
        const target = e.currentTarget;
        const value = target.value;
        this.setState({ comment: value, char_count: 500 - value.length });
    }

    cancel = (e: any) => {
        e.preventDefault();
        this.setState({ comment: '', comment_error: '', char_count: 500 });
        this.props.actionEdit(e);
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        const comment = this.state.comment;

        if (comment.trim().length === 0) {
            this.setState({ comment_error: 'Comment must not be left blank or empty' });
            return;
        }

        const data = JSON.stringify({
            comment,
        });

        fetch(`${API_URL}/comments/${this.state.id}`, {
            method: 'put',
            body: data,
            headers: {
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
                'Content-Type': 'application/json',
            },
        })
        .then(res => {
            if (!res.ok) throw res;
            this.props.actionEdit(e);
            this.props.updateComment(this.state.comment);
            this.setState({ comment: '', comment_error: '', char_count: 500 });
        })
        .catch(error => {
            handleError(error)
            .then(error => {
                this.setState({
                    ...error,
                });
            });
        });
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <FormGroup>
                    <div className="spaced">
                        <Label>Comment</Label>
                        <span className='counter'>{this.state.char_count}</span>
                    </div>
                    <textarea
                        className="form-control"
                        rows={this.state.comment.length > 0 ? 4 : 2}
                        onChange={this.onChange}
                        value={this.state.comment}
                        maxLength={500}
                    ></textarea>
                    {this.state.comment_error.length > 0 &&
                        <ErrorMessage>{this.state.comment_error}</ErrorMessage>
                    }
                </FormGroup>
                {this.state.comment.length > 0 &&
                    <FormGroup className="reversed">
                        <button className="btn-submit">Submit</button>
                        <button
                            className="btn-link"
                            onClick={this.cancel}
                        >
                            Cancel
                        </button>
                    </FormGroup>
                }
            </form>
        )
    }
}