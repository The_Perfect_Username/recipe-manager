import React, { Component } from 'react';
import { handleError } from '../../utils/ErrorHandling';
import {
    FormGroup,
    ErrorMessage,
    API_URL,
    LabelWithCounter,
} from '../Form/Form';

export default class CommentForm extends Component<any> {
    state = {
        recipe_id: '',
        method: 'post',
        comment: '',
        char_count: 500,
        comment_error: '',
    }

    componentWillReceiveProps(props: any) {
        this.setState({ recipe_id: props.recipe_id });
    }

    onChange = (e: any) => {
        const target = e.currentTarget;
        const value = target.value;
        this.setState({ comment: value, char_count: 500 - value.length });
    }

    cancel = (e: any) => {
        e.preventDefault();
        this.setState({ comment: '', comment_error: '', char_count: 500 });
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        const comment = this.state.comment;

        if (comment.trim().length === 0) {
            this.setState({ comment_error: 'Comment must not be left blank or empty' });
            return;
        }

        const data = JSON.stringify({
            recipe_id: this.state.recipe_id,
            comment,
        });

        fetch(`${API_URL}/comments`, {
            method: this.state.method,
            body: data,
            headers: {
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
                'Content-Type': 'application/json',
            },
        })
        .then(res => {
            if (!res.ok) throw res;
            return res.json();
        })
        .then(response => {
            let user: any = window.localStorage.getItem('user');

            user = JSON.parse(user);

            const author = user.name || '';
            const display_name = user.display_name || null;

            response.author = author;
            response.display_name = display_name;
            response.belongs_to_user = true;

            this.setState({ comment: '', comment_error: '', char_count: 500 });
            this.props.setResults(response);
        })
        .catch(error => {
            handleError(error)
            .then(error => {
                this.setState({
                    ...error,
                });
            });
        });
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <FormGroup>
                    <LabelWithCounter
                        label="Comment"
                        counter={this.state.char_count}
                    />
                    <textarea
                        className="form-control"
                        rows={this.state.comment.length > 0 ? 4 : 2}
                        onChange={this.onChange}
                        value={this.state.comment}
                        maxLength={500}
                    ></textarea>
                    {this.state.comment_error.length > 0 &&
                        <ErrorMessage>{this.state.comment_error}</ErrorMessage>
                    }
                </FormGroup>
                {this.state.comment.length > 0 &&
                    <FormGroup className="reversed">
                        <button className="btn-submit">Submit</button>
                        <button
                            className="btn-link"
                            onClick={this.cancel}
                        >
                            Cancel
                        </button>
                    </FormGroup>
                }
            </form>
        )
    }
}
