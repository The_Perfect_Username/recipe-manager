import React from 'react';
import { Redirect } from 'react-router-dom';

export const userLoggedIn = () => {
    const token = window.localStorage.getItem('token') || null;
    return !!token;
}

const AuthenticationRequired = (component: any) => {
    const isLoggedIn: boolean = userLoggedIn();

    if (isLoggedIn) {
        return React.cloneElement(component);
    } else {
        return <Redirect to='/signup' />;
    }
}

export const AvoidEntry = (component: any) => {
    const isLoggedIn: boolean = userLoggedIn();

    if (!isLoggedIn) {
        return React.cloneElement(component);
    } else {
        return <Redirect to='/' />;
    }
}

export const logout = (e?: any) => {
    if (e) e.preventDefault();
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('user');
    window.location.reload();
}

export default AuthenticationRequired;