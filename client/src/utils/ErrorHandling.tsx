export const handleError = async (error: any) => {
    try {
        const hasJSON = 'json' in error;

        if (!hasJSON) throw error;

        const response = await error.json();

        return response;

    } catch (error) {
        console.error(error);
        return { error: "An unexpected error occurred" };
    }
}
